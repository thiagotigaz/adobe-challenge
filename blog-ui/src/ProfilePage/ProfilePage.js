import withLayout from "../hocs/with-layout.hoc";
import DefaultLayout from "../components/DefaultLayout";
import {Button, Card, Col, Form, Row} from "react-bootstrap";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {blogActions, userActions} from "../context/actions";

const ProfilePage = () => {
    const dispatch = useDispatch();
    const {user} = useSelector((state) => state.authentication)
    const [validated, setValidated] = useState(false);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [shouldEditBlog, setShouldEditBlog] = useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();
        const form = e.currentTarget;
        if (form.checkValidity() === false) {
            e.stopPropagation();
        } else {
            if (shouldEditBlog) {
                dispatch(blogActions.updateBlog(user.blog.id, {title, description}))
            }
            dispatch(userActions.updateUser(user.id, {firstName, lastName}))
        }

        setValidated(true);
    };

    useEffect(() => {
        setFirstName(user.firstName);
        setLastName(user.lastName);
        if (user.blog) {
            setShouldEditBlog(user.blog !== null);
            setTitle(user.blog.title);
            setDescription(user.blog.description);
        }
    }, [user])

    return (
        <Row className="justify-content-center mt-2">
            <Col md={6}>
                <Card>
                    <Card.Header as="h5">Edit Profile</Card.Header>
                    <Card.Body>
                        <Form noValidate validated={validated} onSubmit={handleSubmit}>
                            <Form.Group className="mb-3" controlId="formBasicFirstName">
                                <Form.Label>First Name</Form.Label>
                                <Form.Control placeholder="First Name" required value={firstName}
                                              onChange={(e) => setFirstName(e.target.value)}/>
                                <Form.Control.Feedback type="invalid">
                                    First Name is required
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicLastName">
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control placeholder="Last Name" required value={lastName}
                                              onChange={(e) => setLastName(e.target.value)}/>
                                <Form.Control.Feedback type="invalid">
                                    Last Name is required
                                </Form.Control.Feedback>
                            </Form.Group>
                            {shouldEditBlog &&
                            <>
                                <Form.Group className="mb-3" controlId="formBasicBlogTitle">
                                    <Form.Label>Blog Title</Form.Label>
                                    <Form.Control placeholder="Blog Title" required value={title}
                                                  onChange={(e) => setTitle(e.target.value)}/>
                                    <Form.Control.Feedback type="invalid">
                                        Blog Title is required
                                    </Form.Control.Feedback>
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="formBasicBlogDescription">
                                    <Form.Label>Blog Description</Form.Label>
                                    <Form.Control placeholder="Blog Description" required value={description}
                                                  onChange={(e) => setDescription(e.target.value)}/>
                                    <Form.Control.Feedback type="invalid">
                                        Blog Description is required
                                    </Form.Control.Feedback>
                                </Form.Group>
                            </>
                            }
                            <Button variant="primary" type="submit" className="float-end">
                                Save Profile
                            </Button>
                        </Form>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}
export default withLayout(DefaultLayout)(ProfilePage);
