import withLayout from "../hocs/with-layout.hoc";
import DefaultLayout from "../components/DefaultLayout";
import {Button, Card, Col, Form, Row} from "react-bootstrap";
import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {userActions} from "../context/actions";

const RegisterPage = () => {
    const dispatch = useDispatch();
    const [validated, setValidated] = useState(false);
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [shouldCreateBlog, setShouldCreateBlog] = useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();
        const form = e.currentTarget;
        if (form.checkValidity() === false) {
            e.stopPropagation();
        } else {
            dispatch(userActions.register(
                shouldCreateBlog, {username, password, firstName, lastName}, {title, description})
            );
        }

        setValidated(true);
    };

    return (
        <Row className="justify-content-center mt-2">
            <Col md={6}>
                <Card>
                    <Card.Header as="h5">User Registration</Card.Header>
                    <Card.Body>
                        <Form noValidate validated={validated} onSubmit={handleSubmit}>
                            <Form.Group className="mb-3" controlId="formBasicUsername">
                                <Form.Label>Username</Form.Label>
                                <Form.Control placeholder="Username" required
                                              onChange={(e) => setUsername(e.target.value)}/>
                                <Form.Control.Feedback type="invalid">
                                    User is required
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password" required
                                              onChange={(e) => setPassword(e.target.value)}/>
                                <Form.Control.Feedback type="invalid">
                                    Password is required
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicFirstName">
                                <Form.Label>First Name</Form.Label>
                                <Form.Control placeholder="First Name" required
                                              onChange={(e) => setFirstName(e.target.value)}/>
                                <Form.Control.Feedback type="invalid">
                                    First Name is required
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicLastName">
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control placeholder="Last Name" required
                                              onChange={(e) => setLastName(e.target.value)}/>
                                <Form.Control.Feedback type="invalid">
                                    Last Name is required
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Check
                                type="switch"
                                onChange={() => setShouldCreateBlog(!shouldCreateBlog)}
                                id="createblog"
                                label="Would you like to create a blog as well?"
                            />
                            {shouldCreateBlog &&
                            <>
                                <Form.Group className="mb-3" controlId="formBasicBlogTitle">
                                    <Form.Label>Blog Title</Form.Label>
                                    <Form.Control placeholder="Blog Title" required
                                                  onChange={(e) => setTitle(e.target.value)}/>
                                    <Form.Control.Feedback type="invalid">
                                        Blog Title is required
                                    </Form.Control.Feedback>
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="formBasicBlogDescription">
                                    <Form.Label>Blog Description</Form.Label>
                                    <Form.Control placeholder="Blog Description" required
                                                  onChange={(e) => setDescription(e.target.value)}/>
                                    <Form.Control.Feedback type="invalid">
                                        Blog Description is required
                                    </Form.Control.Feedback>
                                </Form.Group>
                            </>
                            }
                            <Button variant="primary" type="submit" className="float-end">
                                Register
                            </Button>
                        </Form>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}
export default withLayout(DefaultLayout)(RegisterPage);
