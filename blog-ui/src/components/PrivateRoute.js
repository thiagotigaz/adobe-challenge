import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {userService} from "../services";

export const PrivateRoute = ({component: Component, ...rest}) => (
    <Route {...rest} render={props => (
        userService.getLocalStorageUser()
            ? <Component {...props} />
            : <Redirect to={{pathname: '/login', state: {from: props.location}}}/>
    )}/>
)
