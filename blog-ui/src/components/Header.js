import React, {useState} from 'react';
import {Container, Form, FormControl, Nav, Navbar, NavDropdown} from "react-bootstrap";
import {useSelector} from "react-redux";
import {history} from '../helpers';

const Header = () => {
    const {user} = useSelector((state) => state.authentication)
    const [search, setSearch] = useState('');
    const handleSearch = (e) => {
        e.preventDefault();
        history.push(`/?search=${search}`)
        window.location.reload();
    }

    return (
        <Navbar bg="primary" variant="dark" expand="lg">
            <Container>
                <Navbar.Brand href="/">Adobe Blog Challenge</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Form className="me-auto w-50" onSubmit={handleSearch}>
                        <FormControl
                            value={search}
                            onChange={(e) => setSearch(e.target.value)}
                            type="search"
                            placeholder="Search Here"
                            aria-label="Search"
                        />
                    </Form>
                    <Nav className="">
                        {!user &&
                        <>
                            <Nav.Link href="/signin">Sign In</Nav.Link>
                            <Nav.Link href="/login">Login</Nav.Link>
                        </>
                        }
                        {user &&
                        <>
                            {user.blog &&
                            <Nav.Link href="/post">+ Create Post</Nav.Link>
                            }
                            <NavDropdown title={`${user.firstName} ${user.lastName}`} id="basic-nav-dropdown">
                                {user.blog &&
                                <NavDropdown.Item href={`/blog/${user.blog.id}`}>My Blog</NavDropdown.Item>
                                }
                                <NavDropdown.Item href="/profile">Profile</NavDropdown.Item>
                                <NavDropdown.Divider/>
                                <NavDropdown.Item href="/login">Logout</NavDropdown.Item>
                            </NavDropdown>
                        </>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
};
export default Header;
