import React, {useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import {useDispatch, useSelector} from "react-redux";
import {history} from "../helpers";
import {alertActions} from "../context/actions";
import {Alert, Col, Container, Row} from "react-bootstrap";

const DefaultLayout = ({children, hdr}) => {
    const Header = hdr;
    const mounted = useRef();
    const dispatch = useDispatch();
    const alert = useSelector((state) => state.alert);
    const [show, setShow] = useState(true);

    useEffect(() => {
        if (!mounted.current) {
            // componentDidMount
            mounted.current = true;
            history.listen((location, action) => {
                // clear alert on location change
                dispatch(alertActions.clear());
            });
        }
    }, [])

    return (
        <>
            <Header/>
            <Container fluid className="pb-2">
                <Row>
                    <Col sm={2} md={1} lg={2}/>
                    <Col xs={12} md={10} lg={8} className="p-0">
                        {alert.message && show &&
                        <Alert variant={alert.type} onClose={() => setShow(false)} dismissible>
                            {alert.message}
                        </Alert>
                        }
                        {children}
                    </Col>
                    <Col sm={2} md={1} lg={2}/>
                </Row>
            </Container>
        </>
    );
};
DefaultLayout.propTypes = {
    children: PropTypes.element.isRequired,
    hdr: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.func,
    ]).isRequired,
};
export default DefaultLayout;
