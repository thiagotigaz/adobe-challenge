import React from 'react';
import {NavLink} from "react-bootstrap";
import withCondition from "../hocs/with-condition";

const PostPreview = ({post}) => {
    return (
        <div>
            <h5>
                <NavLink href={`/blog/${post.blogId}/post/${post.id}`} className="ps-0 mb-2">{post.title}</NavLink>
            </h5>
            <small>{post.metadata}</small>
            <div className="mb-0">{post.content.replace(/<\/?[^>]+(>|$)/g, "")}</div>
            {post.truncated &&
            <NavLink href={`/blog/${post.blogId}/post/${post.id}`} className="ps-0 mb-2">Read More</NavLink>
            }
        </div>
    )
}
export default withCondition(({post}) => post)(PostPreview);