import withCondition from "../hocs/with-condition";
import React from "react";
import {Button, Col, Row} from "react-bootstrap";

const Comment = ({user, comment, handleDelete}) => {

    const deleteComment = (e) => {
        e.preventDefault();
        handleDelete(comment.id);
    };

    return (
        <Row className="justify-content-center">
            <Col md={6}>
                <hr/>
                <strong>{comment.user.firstName} {comment.user.lastName}</strong> - <small>{new Date(comment.dateCreated).toLocaleString()}</small>
                {user && user.id === comment.user.id &&
                <Button variant="danger" size="sm" className="float-end" onClick={deleteComment}>
                    Delete
                </Button>
                }
                <br/>
                {comment.content}
            </Col>
        </Row>
    )
};
export default withCondition(({comment}) => comment)(Comment);
