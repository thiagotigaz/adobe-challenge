import withCondition from "../hocs/with-condition";
import React, {useState} from "react";
import {Button, Col, Form, Row} from "react-bootstrap";

const CreateComment = ({user, post, handleSubmit}) => {
    const [content, setContent] = useState('');
    const [validated, setValidated] = useState(false);

    const createComment = (e) => {
        e.preventDefault();
        const form = e.currentTarget;
        setValidated(true);
        if (form.checkValidity() === false) {
            e.stopPropagation();
        } else {
            handleSubmit(content);
            setContent('');
            setValidated(false);
        }
    };

    return (
        <Row className="justify-content-center">
            <Col md={6}>
                <Form noValidate validated={validated} onSubmit={createComment}>
                    <Form.Group className="mb-3" controlId="formBasicContent">
                        <Form.Control as="textarea" rows={3} placeholder="Content" required value={content}
                                      onChange={(e) => setContent(e.target.value)}/>
                        <Form.Control.Feedback type="invalid">
                            Content is required
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Create Comment
                    </Button>
                </Form>
            </Col>
        </Row>
    )
};
export default withCondition(({user, post, handleSubmit}) => post && user && handleSubmit)(CreateComment);
