import React, {useEffect} from 'react';
import DefaultLayout from "../components/DefaultLayout";
import withLayout from "../hocs/with-layout.hoc";
import {Col, ListGroup, Row} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {useLocation} from "react-router-dom";
import {blogActions} from '../context/actions';
import PostPreview from "../components/PostPreview";


const HomePage = (props) => {
    const dispatch = useDispatch();
    const latestPosts = useSelector((state) => state.blog.latestPosts);
    const latestBlogs = useSelector((state) => state.blog.latestBlogs);

    function useQuery() {
        return new URLSearchParams(useLocation().search);
    }

    let query = useQuery();

    useEffect(() => {
        const searchQuery = query.get('search');
        dispatch(blogActions.getLatestBlogsAndPosts(searchQuery));
    }, [])

    return (
        <Row className={'mt-2'}>
            <Col md={8}>
                <h3>Latest Posts</h3>
                {latestPosts && latestPosts.map((post) => (<PostPreview key={post.id} post={post}/>))}
            </Col>
            <Col>
                <h3>Newest Blogs</h3>
                <ListGroup>
                    {latestBlogs && latestBlogs.map(
                        (blog) => (
                            <ListGroup.Item action variant="secondary" key={blog.id} href={'/blog/' + blog.id}>
                                <strong>{blog.title}</strong><br/>
                                <small>{blog.description}</small>
                            </ListGroup.Item>
                        ),
                    )}
                </ListGroup>
            </Col>
        </Row>
    )
}
export default withLayout(DefaultLayout)(HomePage);
