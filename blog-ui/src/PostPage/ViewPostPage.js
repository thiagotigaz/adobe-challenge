import React, {useEffect, useRef, useState} from 'react';
import {Button, Col, Row} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {blogActions} from '../context/actions';
import CreateComment from "../components/CreateComment";
import withLayout from "../hocs/with-layout.hoc";
import DefaultLayout from "../components/DefaultLayout";
import Comment from "../components/Comment";

const ViewPostPage = (props) => {
    const dispatch = useDispatch();
    const mounted = useRef();
    const {post, comments} = useSelector((state) => state.blog);
    const {user} = useSelector((state) => state.authentication);
    const [blogId, setBlogId] = useState();
    const [postId, setPostId] = useState();

    useEffect(() => {
        if (!mounted.current) {
            // componentDidMount
            mounted.current = true;
            setBlogId(props.match.params.blogId);
            setPostId(props.match.params.postId);
        } else if (blogId) {
            dispatch(blogActions.getPostAndComments(blogId, postId));
        }
    }, [blogId, postId]);

    const createComment = (content) => {
        dispatch(blogActions.createComment(blogId, postId, {content}))
    }

    const deleteComment = (commentId) => {
        dispatch(blogActions.deleteComment(blogId, postId, commentId))
    }

    const deletePost = () => {
        if (window.confirm('Are you sure you want to delete this post?')) {
            dispatch(blogActions.deletePost(blogId, postId))
        }
    }

    return (
        <>
            {post &&
            <>
                <Row className="pt-2">
                    <Col>
                        <h5 className="d-inline-block mb-0">{post.title}</h5>
                        <p className="d-inline">
                            <small> - {post.metadata} - {new Date(post.dateCreated).toLocaleString()}</small>
                        </p>

                        {user && user.blog && user.blog.id === post.blogId &&
                        <>
                            <Button variant="danger" size="sm" className="float-end" onClick={() => deletePost()}>
                                Delete
                            </Button>
                            <Button href={`/post/${post.id}`} variant="secondary" size="sm" className="float-end me-1">
                                Edit
                            </Button>
                        </>
                        }
                    </Col>
                </Row>
                <hr/>
                <Row>
                    <Col dangerouslySetInnerHTML={{__html:post.content}}/>
                </Row>
                <CreateComment post={post} user={user} handleSubmit={createComment}/>
                {comments &&
                comments.content.map((comment) => (
                    <Comment key={comment.id} user={user} comment={comment} handleDelete={deleteComment}/>))
                }
            </>
            }
        </>
    )
};
export default withLayout(DefaultLayout)(ViewPostPage);
