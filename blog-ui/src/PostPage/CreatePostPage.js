import React, {useEffect, useRef, useState} from 'react';
import {convertToRaw, EditorState} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import {stateFromHTML} from 'draft-js-import-html';
import {Button, Card, Col, Form, Row} from 'react-bootstrap';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import DefaultLayout from "../components/DefaultLayout";
import withLayout from "../hocs/with-layout.hoc";
import {useDispatch, useSelector} from "react-redux";
import {blogActions} from "../context/actions";

const CreatePostPage = (props) => {
    const dispatch = useDispatch();
    const mounted = useRef();
    const {blog} = useSelector((state) => state.authentication.user)
    const {post} = useSelector((state) => state.blog)
    const [postId, setPostId] = useState();
    const [validated, setValidated] = useState(false);
    const [title, setTitle] = useState('');
    const [metadata, setMetadata] = useState('');
    const [content, setContent] = useState('');
    const [editorState, setEditorState] = useState(EditorState.createEmpty());

    const onEditorStateChange = (editorState) => {
        setEditorState(editorState);
        setContent(draftToHtml(convertToRaw(editorState.getCurrentContent())));
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const form = e.currentTarget;
        if (form.checkValidity() === false) {
            e.stopPropagation();
        } else {
            setContent(draftToHtml(convertToRaw(editorState.getCurrentContent())));
            console.log(JSON.stringify(content));
            if (post) {
                dispatch(blogActions.updatePost(blog.id, post.id, {title, metadata, content}))
            } else {
                dispatch(blogActions.createPost(blog.id, {title, metadata, content}))
            }
        }

        setValidated(true);
    };

    useEffect(() => {
        if (!mounted.current) {
            console.log('useEffect getPost')
            // componentDidMount
            mounted.current = true;
            setPostId(props.match.params.postId);
        }

        if (postId) {
            if (post) {
                console.log(JSON.stringify(post))
                setTitle(post.title);
                setMetadata(post.metadata);
                setContent(post.content);
                setEditorState(EditorState.createWithContent(stateFromHTML(post.content)));
            } else {

                dispatch(blogActions.getBlogPost(blog.id, postId));
            }
        }
    }, [postId, post])

    return (
        <Row className="justify-content-center mt-2">
            <Col>
                <Card>
                    <Card.Header as="h5">{post ? 'Edit' : 'Create'} Post</Card.Header>
                    <Card.Body>
                        <Form noValidate validated={validated} onSubmit={handleSubmit}>
                            <Form.Group className="mb-3" controlId="formBasicTitle">
                                <Form.Label>Title</Form.Label>
                                <Form.Control placeholder="Title" required value={title}
                                              onChange={(e) => setTitle(e.target.value)}/>
                                <Form.Control.Feedback type="invalid">
                                    Title is required
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicMetadata">
                                <Form.Label>Metadata</Form.Label>
                                <Form.Control placeholder="Metadata" required value={metadata}
                                              onChange={(e) => setMetadata(e.target.value)}/>
                                <Form.Control.Feedback type="invalid">
                                    Metadata is required
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicContent">
                                <Form.Label>Content</Form.Label>
                                <Card>
                                    <Editor
                                        editorState={editorState}

                                        onEditorStateChange={onEditorStateChange}
                                    />
                                </Card>
                                <Form.Control as="textarea" rows={15} placeholder="Content" required value={content}
                                              className="d-none"
                                              onChange={(e) => setContent(e.target.value)}/>
                                <Form.Control.Feedback type="invalid">
                                    Content is required
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Button variant="primary" type="submit" className="float-end">
                                {post ? 'Save Post' : 'Create Post'}
                            </Button>
                        </Form>
                    </Card.Body>
                </Card>

            </Col>
        </Row>
    )
}
export default withLayout(DefaultLayout)(CreatePostPage);
