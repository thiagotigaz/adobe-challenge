import axios from 'axios';
import { configConstants } from '../contants';

const client = axios.create({
    baseURL: configConstants.API_URL,
});
export default client;