export const pending = (action) => `${action}_PENDING`;
export const fulfilled = (action) => `${action}_FULFILLED`;
export const rejected = (action) => `${action}_REJECTED`;
