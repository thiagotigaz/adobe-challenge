import {userService} from '../services'

export function authHeader() {
    // return authorization header with jwt token
    const user = userService.getLocalStorageUser();
    if (user && user.token) {
        return { 'Authorization': 'Bearer ' + user.token };
    } else {
        return {};
    }
}