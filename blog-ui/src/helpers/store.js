import {applyMiddleware, compose, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import promise from 'redux-promise-middleware';

import rootReducer from '../context/reducers';

const composeEnhancers = typeof window !== 'undefined'
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
    : compose;

export const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(promise, thunkMiddleware)),
);