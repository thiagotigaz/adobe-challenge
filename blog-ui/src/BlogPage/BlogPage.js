import React, {useEffect, useRef, useState} from 'react';
import DefaultLayout from "../components/DefaultLayout";
import withLayout from "../hocs/with-layout.hoc";
import {Button, Col, Row} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {blogActions} from '../context/actions';
import withCondition from "../hocs/with-condition";
import PostPreview from "../components/PostPreview";

const BlogPage = (props) => {
    const dispatch = useDispatch();
    const mounted = useRef();
    const {blog, posts} = useSelector((state) => state.blog);
    const {user} = useSelector((state) => state.authentication);
    const [blogId, setBlogId] = useState();

    useEffect(() => {
        if (!mounted.current) {
            // componentDidMount
            mounted.current = true;
            setBlogId(props.match.params.id);
        } else if (blogId) {
            dispatch(blogActions.getBlogAndPosts(blogId));
        }
    }, [blogId]);

    const CreateFirstPost = withCondition(({user, blog, posts}) =>
        blog && user && posts
        && blog.author.id === user.id
        && posts.content && posts.content.length === 0
    )(() => (
        <Row className="justify-content-center">
            <Col md={4}>
                <Button className="btn-lg d-block" href="/post">+ Create Your First Post</Button>
            </Col>
        </Row>
    ));

    return (
        <>
            <Row className="pt-2">
                <Col>
                    {blog &&
                    <p className="mb-0">
                        <h5 className="d-inline-block mb-0">Welcome to '{blog.title}'</h5>
                        <small> - {blog.description}</small>
                    </p>
                    }
                </Col>
            </Row>
            <hr/>
            <CreateFirstPost user={user} blog={blog} posts={posts}/>
            <Row>
                <Col>
                    {posts &&
                        posts.content.map((post) => (<PostPreview key={post.id} post={post}/>))
                    }
                </Col>
            </Row>
        </>
    )
}
export default withLayout(DefaultLayout)(BlogPage);