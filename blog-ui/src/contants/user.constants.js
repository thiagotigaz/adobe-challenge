export const userConstants = {
    LOCAL_STORAGE_USER: 'user',
    REGISTER: 'REGISTER',
    UPDATE_USER: 'UPDATE_USER',
    LOGIN: 'LOGIN',
    LOGOUT: 'USERS_LOGOUT',
};
