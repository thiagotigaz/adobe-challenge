import {history} from '../helpers';

import {Redirect, Route, Router, Switch} from "react-router-dom";
import {PrivateRoute} from "../components";
import {LoginPage} from "../LoginPage";
import {HomePage} from "../HomePage";
import RegisterPage from "../RegisterPage/RegisterPage";
import BlogPage from "../BlogPage/BlogPage";
import {CreatePostPage, ViewPostPage} from "../PostPage";
import ProfilePage from "../ProfilePage/ProfilePage";

function App() {
    return (
        <Router history={history}>
            <Switch>
                <Route exact path="/" component={HomePage}/>
                <Route exact path="/login" component={LoginPage}/>
                <Route exact path="/signin" component={RegisterPage}/>
                <Route exact path="/blog/:id" component={BlogPage}/>
                <Route exact path="/blog/:blogId/post/:postId" component={ViewPostPage}/>
                <PrivateRoute exact path="/profile" component={ProfilePage}/>
                <PrivateRoute exact path="/post" component={CreatePostPage}/>
                <PrivateRoute exact path="/post/:postId" component={CreatePostPage}/>
                <Redirect from="*" to="/"/>
            </Switch>
        </Router>
    );
}

export default App;
