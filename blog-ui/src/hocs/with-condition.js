import React from 'react';

const withCondition = (conditionalRenderingFn, FallbackComponent) => (WrappedComponent) => {
  const WithCondition = (props) => {
    if (conditionalRenderingFn(props)) {
      // eslint-disable-next-line react/jsx-props-no-spreading
      return <WrappedComponent {...props} />;
    }
    if (FallbackComponent) {
      return <FallbackComponent />;
    }
    return null;
  };
  const wrappedComponentName = WrappedComponent.displayName || WrappedComponent.name || 'Component';
  WithCondition.displayName = `withCondition(${wrappedComponentName})`;
  return WithCondition;
};
export default withCondition;
