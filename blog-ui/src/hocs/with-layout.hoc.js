import React from 'react';
import Header from "../components/Header";

const withLayout = (LayoutComponent, HeaderComponent = Header) => (WrappedComponent) => {
  const WithLayout = (props) => (
      <LayoutComponent hdr={HeaderComponent}>
        {/* eslint-disable-next-line react/jsx-props-no-spreading */}
        <WrappedComponent {...props} />
      </LayoutComponent>
  );
  const wrappedComponentName = WrappedComponent.displayName || WrappedComponent.name || 'Component';
  WithLayout.displayName = `withLayout(${wrappedComponentName})`;
  return WithLayout;
};
export default withLayout;