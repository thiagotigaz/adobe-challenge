import {userConstants} from '../contants';
import {apiClient, authHeader} from '../helpers';

const getLocalStorageUser = () => {
    const userString = localStorage.getItem(userConstants.LOCAL_STORAGE_USER);
    return JSON.parse(userString);
}

const setLocalStorageUser = (user) => {
    localStorage.setItem(userConstants.LOCAL_STORAGE_USER, JSON.stringify(user));
}

const removeLocalStorageUser = () => {
    localStorage.removeItem(userConstants.LOCAL_STORAGE_USER);
}

const login = (username, password) => {
    return apiClient.post('/auth/login', {username, password})
        .then(handleLoginSuccess)
        .catch(handleLoginError);
};

const handleLoginSuccess = (res, blog) => {
    const user = {...res.data, token: res.headers.authorization};
    setLocalStorageUser(user);
    return user;
};

const handleLoginError = (error) => {
    if (error.response.status === 401) {
        // auto logout if 401 response returned from api
        logout();
        // window.location.reload(true);
        return Promise.reject('Username or Password invalid!')
    }
    return Promise.reject(error);
};

// remove user from local storage to log user out
const logout = removeLocalStorageUser;
const register = (user) => apiClient.post('/v1/users', user);

const update = (userId, user) => apiClient.put(`/v1/users/${userId}`, user, {headers: authHeader()})
    .then((res) => {
        const localUser = getLocalStorageUser();
        localUser.firstName = user.firstName;
        localUser.lastName = user.lastName;
        setLocalStorageUser(localUser)
        return Promise.resolve(localUser);
    });

export const userService = {
    getLocalStorageUser,
    setLocalStorageUser,
    removeLocalStorageUser,
    login,
    handleLoginSuccess,
    logout,
    register,
    update,
};