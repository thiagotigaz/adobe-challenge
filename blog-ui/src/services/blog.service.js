import {apiClient} from "../helpers";

const getLatestBlogs = () => apiClient.get('/v1/latest/blogs').then((res) => res.data);

const getLatestPosts = (search) => {
    return apiClient.get('/v1/latest/posts',{params: {search}}).then((res) => res.data);
};

export const blogService = {
    getLatestBlogs,
    getLatestPosts,
};
