import React, {useEffect, useRef, useState} from 'react';
import {useDispatch} from 'react-redux';

import {userActions} from '../context/actions';
import withLayout from "../hocs/with-layout.hoc";
import DefaultLayout from "../components/DefaultLayout";
import Header from "../components/Header";
import {Button, Card, Col, Form, Row} from "react-bootstrap";

const LoginPage = () => {
    const mounted = useRef();
    const dispatch = useDispatch();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [submitted, setSubmitted] = useState(false);

    useEffect(() => {
        if (!mounted.current) {
            // componentDidMount
            mounted.current = true;
            dispatch(userActions.logout())
        }
    }, [])

    const handleSubmit = (e) => {
        e.preventDefault();
        setSubmitted(true)
        if (username && password) {
            dispatch(userActions.login(username, password));
        }
    }

    return (
        <Row className="justify-content-center mt-2">
            <Col md={6}>
                <Card>
                    <Card.Header as="h5">Login</Card.Header>
                    <Card.Body>
                        <Form onSubmit={handleSubmit}>
                            <Form.Group className="mb-3" controlId="formBasicUsername">
                                <Form.Label>Username</Form.Label>
                                <Form.Control placeholder="Username" onChange={(e) => setUsername(e.target.value)}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password"
                                              onChange={(e) => setPassword(e.target.value)}/>
                            </Form.Group>
                            <Button variant="primary" type="submit">
                                Login
                            </Button>
                        </Form>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    );
}
export default withLayout(DefaultLayout)(LoginPage, Header);
