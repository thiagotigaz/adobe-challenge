import {blogConstants} from '../../contants';
import {blogService, userService} from '../../services';
import {apiClient, authHeader} from "../../helpers";
import {history} from '../../helpers';

const getLatestBlogs = (dispatch) => dispatch({
    type: blogConstants.GET_LATEST_BLOGS,
    payload: blogService.getLatestBlogs()
});

const getLatestPosts = (search) => (dispatch) => dispatch({
    type: blogConstants.GET_LATEST_POSTS,
    payload: blogService.getLatestPosts(search)
});

const getLatestBlogsAndPosts = (search) => (dispatch) => {
    dispatch(getLatestBlogs)
    dispatch(getLatestPosts(search))
}

const createBlog = (blog) => (dispatch) => dispatch({
    type: blogConstants.CREATE_BLOG,
    payload: apiClient.post('/v1/blogs', blog, {
        headers: authHeader()
    }).then((res) => {
        const parts = res.headers.location.split('/');
        blog.id = parts[parts.length - 1];
        const user = userService.getLocalStorageUser()
        user.blog = blog;
        userService.setLocalStorageUser(user)
        history.push(`/blog/${blog.id}`);
        return Promise.resolve(user);
    })
})

const getBlog = (blogId) => (dispatch) => dispatch({
    type: blogConstants.GET_BLOG,
    payload: apiClient.get(`/v1/blogs/${blogId}`)
        .then((res) => res.data)
})


const updateBlog = (blogId, blog) => (dispatch) => dispatch({
    type: blogConstants.UPDATE_BLOG,
    payload: apiClient.put(`/v1/blogs/${blogId}`, blog, {
        headers: authHeader()
    }).then((res) => {
        const localUser = userService.getLocalStorageUser();
        localUser.blog.title = blog.title;
        localUser.blog.description = blog.description;
        userService.setLocalStorageUser(localUser)
        return Promise.resolve(localUser);
    })
})

const getBlogPosts = (blogId) => (dispatch) => dispatch({
    type: blogConstants.GET_BLOG_POSTS,
    payload: apiClient.get(`/v1/blogs/${blogId}/posts`)
        .then((res) => res.data)
})

const getBlogAndPosts = (blogId) => (dispatch) => {
    dispatch(getBlog(blogId))
    dispatch(getBlogPosts(blogId))
}

const getBlogPost = (blogId, postId) => (dispatch) => dispatch({
    type: blogConstants.GET_BLOG_POST,
    payload: apiClient.get(`/v1/blogs/${blogId}/posts/${postId}`)
        .then((res) => res.data)
})

const createPost = (blogId, post) => (dispatch) => dispatch({
    type: blogConstants.CREATE_POST,
    payload: apiClient.post(`/v1/blogs/${blogId}/posts`, post, {
        headers: authHeader()
    }).then((res) => {
        const parts = res.headers.location.split('/');
        const postId = parts[parts.length - 1];
        history.push(`/blog/${blogId}/post/${postId}`)
        return res;
    })
})

const updatePost = (blogId, postId, post) => (dispatch) => dispatch({
    type: blogConstants.UPDATE_POST,
    payload: apiClient.put(`/v1/blogs/${blogId}/posts/${postId}`, post, {
        headers: authHeader()
    }).then((res) => {
        history.push(`/blog/${blogId}/post/${postId}`)
        return res;
    })
})

const getPostComments = (blogId, postId) => (dispatch) => dispatch({
    type: blogConstants.GET_POST_COMMENTS,
    payload: apiClient.get(`/v1/blogs/${blogId}/posts/${postId}/comments`)
        .then((res) => res.data)
})

const getPostAndComments = (blogId, postId) => (dispatch) => {
    dispatch(getBlogPost(blogId, postId))
    dispatch(getPostComments(blogId, postId))
}

const createComment = (blogId, postId, comment) => (dispatch) => dispatch({
    type: blogConstants.CREATE_COMMENT,
    payload: apiClient.post(`/v1/blogs/${blogId}/posts/${postId}/comments`, comment, {
        headers: authHeader()
    }).then((res) => dispatch(getPostComments(blogId, postId)))
})

const deleteComment = (blogId, postId, commentId) => (dispatch) => dispatch({
    type: blogConstants.DELETE_COMMENT,
    payload: apiClient.delete(`/v1/blogs/${blogId}/posts/${postId}/comments/${commentId}`, {
        headers: authHeader()
    }).then((res) => Promise.resolve(commentId))
})

const deletePost = (blogId, postId) => (dispatch) => dispatch({
    type: blogConstants.DELETE_COMMENT,
    payload: apiClient.delete(`/v1/blogs/${blogId}/posts/${postId}`, {
        headers: authHeader()
    }).then((res) => history.push(`/blog/${blogId}`))
})


export const blogActions = {
    getLatestBlogsAndPosts,
    getLatestBlogs,
    createBlog,
    updateBlog,
    getBlogAndPosts,
    getBlogPost,
    createPost,
    updatePost,
    createComment,
    deleteComment,
    deletePost,
    getPostComments,
    getPostAndComments
};
