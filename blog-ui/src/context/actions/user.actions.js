import {userConstants} from '../../contants';
import {userService} from '../../services';
import {alertActions, blogActions} from './';
import {history} from '../../helpers';

const login = (username, password) => (dispatch) => dispatch(
    {
        type: userConstants.LOGIN,
        payload: userService.login(username, password)
            .then((user) => {
                history.push('/');
                return user;
            })
            .catch((error) => {
                dispatch(alertActions.error(error.toString()));
                throw error;
            })
    }
);

const logout = () => {
    userService.logout();
    return {type: userConstants.LOGOUT};
}

const register = (shouldCreateBlog, user, blog) => (dispatch) => dispatch(
    {
        type: userConstants.REGISTER,
        payload: userService.register(user)
            .then((res) => {
                const user = userService.handleLoginSuccess(res)
                dispatch({type: 'LOGIN_FULFILLED', payload: user})
                if (shouldCreateBlog) {
                    return dispatch(blogActions.createBlog(blog));
                } else {
                    history.push('/');
                    return user;
                }
            })
            .catch((error) => {
                dispatch(alertActions.error(error.toString()));
                throw error;
            })
    }
);

const updateUser = (userId, user) =>(dispatch) => dispatch(
    {
        type: userConstants.UPDATE_USER,
        payload: userService.update(userId, user)
            .then((user) => {
                history.push('/');
                return user;
            })
            .catch((error) => {
                dispatch(alertActions.error(error.toString()));
                throw error;
            })
    }
);

export const userActions = {
    login,
    logout,
    register,
    updateUser,
};
