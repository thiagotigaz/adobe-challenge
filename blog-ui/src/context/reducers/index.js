import {combineReducers} from 'redux';

import {authentication} from './authentication.reducer';
import {users} from './users.reducer';
import {alert} from './alert.reducer';
import {blog} from './blog.reducer';

const rootReducer = combineReducers({
    authentication,
    users,
    blog,
    alert
});

export default rootReducer;