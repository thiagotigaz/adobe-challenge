import {blogConstants} from '../../contants';
import {pending, rejected, fulfilled} from "../../helpers/string-util";

export function blog(state = {}, action) {
    switch (action.type) {
        case pending(blogConstants.GET_LATEST_BLOGS):
        case fulfilled(blogConstants.GET_LATEST_BLOGS):
            return {
                ...state,
                latestBlogs: action.payload
            };
        case rejected(blogConstants.GET_LATEST_BLOGS):
            return {};
        case pending(blogConstants.GET_LATEST_POSTS):
        case fulfilled(blogConstants.GET_LATEST_POSTS):
            return {
                ...state,
                latestPosts: action.payload
            };
        case fulfilled(blogConstants.GET_BLOG):
            return {
                ...state,
                blog: action.payload
            };
        case fulfilled(blogConstants.GET_BLOG_POSTS):
            return {
                ...state,
                posts: action.payload
            };
        case fulfilled(blogConstants.GET_BLOG_POST):
            return {
                ...state,
                post: action.payload
            };
        case fulfilled(blogConstants.GET_POST_COMMENTS):
            return {
                ...state,
                comments: action.payload
            };
        case fulfilled(blogConstants.DELETE_COMMENT):
            return {
                ...state,
                comments: {
                    ...state.comments,
                    content: state.comments.content.filter(c => c.id !== action.payload)
                }
            };
        default:
            return state
    }
}
