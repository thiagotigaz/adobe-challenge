import {blogConstants, userConstants} from '../../contants';
import {pending, rejected, fulfilled} from "../../helpers/string-util";

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? {loggedIn: true, user} : {};

export function authentication(state = initialState, action) {
    switch (action.type) {
        case pending(userConstants.LOGIN):
            return {
                loggingIn: true,
                user: action.payload
            };
        case fulfilled(userConstants.LOGIN):
            return {
                loggedIn: true,
                user: action.payload
            };
        case fulfilled(userConstants.UPDATE_USER):
            return {
                loggedIn: true,
                user: action.payload
            };
        case rejected(userConstants.LOGIN):
            return {};
        case fulfilled(blogConstants.CREATE_BLOG):
            return {
                loggedIn: true,
                user: action.payload
            };
        case userConstants.LOGOUT:
            return {};
        default:
            return state
    }
}