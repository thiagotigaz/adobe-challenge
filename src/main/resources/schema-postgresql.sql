CREATE TABLE IF NOT EXISTS public.blog (
    id bigint NOT NULL,
    background_color character varying(255),
    date_created timestamp without time zone NOT NULL,
    date_updated timestamp without time zone,
    description character varying(255),
    highlight_color character varying(255),
    primary_color character varying(255),
    secondary_color character varying(255),
    title character varying(255) NOT NULL,
    author_id bigint NOT NULL
);

CREATE TABLE IF NOT EXISTS public.comment (
    id bigint NOT NULL,
    content character varying(255) NOT NULL,
    date_created timestamp without time zone NOT NULL,
    date_updated timestamp without time zone,
    post_id bigint NOT NULL,
    user_id bigint NOT NULL
);

CREATE SEQUENCE IF NOT EXISTS public.hibernate_sequence
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;

CREATE TABLE IF NOT EXISTS public.post (
    id bigint NOT NULL,
    content text NOT NULL,
    date_created timestamp without time zone NOT NULL,
    date_updated timestamp without time zone,
    metadata character varying(255),
    title character varying(255) NOT NULL,
    blog_id bigint NOT NULL
);

CREATE TABLE IF NOT EXISTS public.role (
    id bigint NOT NULL,
    authority character varying(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS public.user_account (
    id bigint NOT NULL,
    date_created timestamp without time zone NOT NULL,
    date_updated timestamp without time zone,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    username character varying(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS public.user_role (
    user_id bigint NOT NULL,
    role_id bigint NOT NULL
);

CREATE OR REPLACE FUNCTION create_constraint_if_not_exists (
    t_name text, c_name text, constraint_sql text
)
returns VOID AS
'
BEGIN
    IF NOT EXISTS (SELECT CONSTRAINT_NAME
                   FROM information_schema.constraint_column_usage
                   WHERE table_name = t_name  and constraint_name = c_name) THEN
        EXECUTE constraint_sql;
END IF;
END;
' LANGUAGE PLPGSQL;

-- Primary Keys
SELECT create_constraint_if_not_exists('blog', 'blog_pkey', 'ALTER TABLE ONLY public.blog ADD CONSTRAINT blog_pkey PRIMARY KEY (id);');
SELECT create_constraint_if_not_exists('comment', 'comment_pkey', 'ALTER TABLE ONLY public.comment ADD CONSTRAINT comment_pkey PRIMARY KEY (id);');
SELECT create_constraint_if_not_exists('post', 'post_pkey', 'ALTER TABLE ONLY public.post ADD CONSTRAINT post_pkey PRIMARY KEY (id);');
SELECT create_constraint_if_not_exists('role', 'role_pkey', 'ALTER TABLE ONLY public.role ADD CONSTRAINT role_pkey PRIMARY KEY (id);');
SELECT create_constraint_if_not_exists('user_account', 'user_account_pkey', 'ALTER TABLE ONLY public.user_account ADD CONSTRAINT user_account_pkey PRIMARY KEY (id);');
-- Unique Constraints
SELECT create_constraint_if_not_exists('user_account', 'username_uk', 'ALTER TABLE ONLY public.user_account ADD CONSTRAINT username_uk UNIQUE (username);');
-- Foreign Keys
SELECT create_constraint_if_not_exists('user_account', 'blog_author_fk', 'ALTER TABLE ONLY public.blog ADD CONSTRAINT blog_author_fk FOREIGN KEY (author_id) REFERENCES public.user_account(id);');
SELECT create_constraint_if_not_exists('post', 'comment_post_fk', 'ALTER TABLE ONLY public.comment ADD CONSTRAINT comment_post_fk FOREIGN KEY (post_id) REFERENCES public.post(id);');
SELECT create_constraint_if_not_exists('user_account', 'comment_user_fk', 'ALTER TABLE ONLY public.comment ADD CONSTRAINT comment_user_fk FOREIGN KEY (user_id) REFERENCES public.user_account(id);');
SELECT create_constraint_if_not_exists('blog', 'post_blog_fk', 'ALTER TABLE ONLY public.post ADD CONSTRAINT post_blog_fk FOREIGN KEY (blog_id) REFERENCES public.blog(id);');
SELECT create_constraint_if_not_exists('role', 'user_role_role_fk', 'ALTER TABLE ONLY public.user_role ADD CONSTRAINT user_role_role_fk FOREIGN KEY (role_id) REFERENCES public.role(id);');
SELECT create_constraint_if_not_exists('user_account', 'user_role_user_fk', 'ALTER TABLE ONLY public.user_role ADD CONSTRAINT user_role_user_fk FOREIGN KEY (user_id) REFERENCES public.user_account(id);');

