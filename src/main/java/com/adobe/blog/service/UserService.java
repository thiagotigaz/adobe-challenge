package com.adobe.blog.service;

import com.adobe.blog.dto.user.UserCreateRequest;
import com.adobe.blog.dto.user.UserDto;
import com.adobe.blog.dto.user.UserUpdateRequest;
import com.adobe.blog.exception.UsernameNotAvailableException;
import com.adobe.blog.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface UserService {
    User createUser(UserCreateRequest userCreateRequest) throws UsernameNotAvailableException;

    Page<UserDto> findAll(Pageable pageable);

    Optional<UserDto> findById(Long id);

    void updateUser(User loggedInUser, UserUpdateRequest userDto);
}

