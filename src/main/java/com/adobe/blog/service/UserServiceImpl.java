package com.adobe.blog.service;

import com.adobe.blog.dto.auth.LoggedInUser;
import com.adobe.blog.dto.user.UserCreateRequest;
import com.adobe.blog.dto.user.UserDto;
import com.adobe.blog.dto.user.UserUpdateRequest;
import com.adobe.blog.exception.UsernameNotAvailableException;
import com.adobe.blog.model.User;
import com.adobe.blog.repository.RoleRepository;
import com.adobe.blog.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

import static com.adobe.blog.model.Role.ROLE_USER;


@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepo;
    private final RoleRepository roleRepo;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper;

    /**
     * Finds all users in a pageable fashion
     *
     * @param pageable containing the information to be searched
     * @return pageable containing page information and list of users
     */
    @Override
    public Page<UserDto> findAll(Pageable pageable) {
        return userRepo.findAll(pageable).map(user -> modelMapper.map(user, UserDto.class));
    }

    /**
     * Finds a single user and map to its dto before returning
     *
     * @param id of the user
     * @return user dto containing restricted information
     */
    @Override
    public Optional<UserDto> findById(Long id) {
        Optional<User> userOpt = userRepo.findById(id);
        return userOpt.map(user -> modelMapper.map(user, UserDto.class));
    }

    /**
     * Creates a user with given dto information
     *
     * @param userCreateRequest containing new user information
     * @return new created user
     */
    @Override
    @Transactional
    public User createUser(UserCreateRequest userCreateRequest) throws UsernameNotAvailableException {
        Optional<User> userOpt = userRepo.findByUsername(userCreateRequest.getUsername());
        if (userOpt.isPresent()) {
            throw new UsernameNotAvailableException();
        }
        User user = modelMapper.map(userCreateRequest, User.class);
        user.setPassword(passwordEncoder.encode(userCreateRequest.getPassword()));
        user.setAuthorities(List.of(roleRepo.findByAuthority(ROLE_USER)));
        return userRepo.save(user);
    }

    /**
     * Updates a user with given id and dto information
     *
     * @param loggedInUser
     * @param userUpdateRequest containing existing user information
     */
    @Override
    @Transactional
    public void updateUser(User loggedInUser, UserUpdateRequest userUpdateRequest) {
        Optional<User> userOpt = userRepo.findById(userUpdateRequest.getId());
        if (userOpt.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        if (!(loggedInUser.isAdmin() || userOpt.get().getId().equals(loggedInUser.getId()))) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        User user = userOpt.get();
        user.setFirstName(userUpdateRequest.getFirstName());
        user.setLastName(userUpdateRequest.getLastName());
        String pass = userUpdateRequest.getPassword();
        if (StringUtils.hasText(pass)) {
            if (pass.length() < 8 || pass.length() > 64) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
            user.setPassword(passwordEncoder.encode(userUpdateRequest.getPassword()));
        }
        userRepo.save(user);
    }

}
