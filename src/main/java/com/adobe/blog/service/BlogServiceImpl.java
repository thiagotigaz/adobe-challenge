package com.adobe.blog.service;

import com.adobe.blog.dto.auth.LoggedInUser;
import com.adobe.blog.dto.blog.*;
import com.adobe.blog.model.Blog;
import com.adobe.blog.model.Comment;
import com.adobe.blog.model.Post;
import com.adobe.blog.model.User;
import com.adobe.blog.repository.*;
import com.adobe.blog.util.PostUtil;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static com.adobe.blog.model.Role.ROLE_AUTHOR;
import static org.springframework.util.StringUtils.hasText;


@Service
@RequiredArgsConstructor
public class BlogServiceImpl implements BlogService {


    private static final int MIN_SEARCH_LENGTH = 2;
    public static final String THE_AUTHOR_ALREADY_HAS_A_BLOG_ONLY_ONE_BLOG_PER_USER_IS_ALLOWED = "The author already has a blog. Only one blog per user is allowed.";
    private final UserRepository userRepo;
    private final BlogRepository blogRepo;
    private final PostRepository postRepo;
    private final CommentRepository commentRepo;
    private final RoleRepository roleRepo;
    private final ModelMapper modelMapper;
    private final AuthService authService;

    /**
     * Creates a blog with given dto information
     *
     * @param loggedInUser containing the user information
     * @param blogCreateRequest containing new blog information
     * @return new created blog
     */
    @Override
    @Transactional
    public Blog createBlog(User loggedInUser, BlogCreateRequest blogCreateRequest) {
        Optional<User> authorOpt = userRepo.findById(loggedInUser.getId());
        // User has to exist and can only have one blog
        if (authorOpt.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        User author = authorOpt.get();
        if (author.getBlog() != null) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, THE_AUTHOR_ALREADY_HAS_A_BLOG_ONLY_ONE_BLOG_PER_USER_IS_ALLOWED
            );
        }
        author.getAuthorities().add(roleRepo.findByAuthority(ROLE_AUTHOR));
        Blog blog = modelMapper.map(blogCreateRequest, Blog.class);
        blog.setAuthor(author);

        return blogRepo.save(blog);
    }

    /**
     * Updates a blog with given id and dto information
     *
     * @param loggedInUser
     * @param blogUpdateRequest containing existing blog information
     */
    @Override
    @Transactional
    public void updateBlog(User loggedInUser, BlogUpdateRequest blogUpdateRequest) {
        Optional<Blog> blogOpt = blogRepo.findById(blogUpdateRequest.getId());
        // Blog has to exist
        if (blogOpt.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        // Has to be either admin or the blog owner
        if (!(loggedInUser.isAdmin() || isBlogOwner(blogOpt.get(), loggedInUser.getId()))) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        Blog blog = blogOpt.get();
        modelMapper.map(blogUpdateRequest, blog);
        blogRepo.save(blog);
    }

    static boolean isBlogOwner(Blog blog, Long authorId) {
        return blog.getAuthor().getId().equals(authorId);
    }

    /**
     * Finds a single blog and map to its dto before returning
     *
     * @param id of the blog
     * @return blog dto containing restricted information
     */
    @Override
    public Optional<BlogDto> findBlog(Long id) {
        Optional<Blog> blogOpt = blogRepo.findById(id);
        return blogOpt.map(blog -> modelMapper.map(blog, BlogDto.class));
    }

    /**
     * Finds all blogs in a pageable fashion
     *
     * @param pageable containing the information to be searched
     * @return page containing pagination information and list of blogs
     */
    @Override
    public Page<BlogDto> findBlogs(Pageable pageable) {
        return blogRepo.findAll(pageable).map(blog -> modelMapper.map(blog, BlogDto.class));
    }

    /**
     * Creates a post with given dto information
     *
     *
     * @param loggedInUser
     * @param postCreateRequest containing new post information
     * @return new created post
     */
    @Override
    @Transactional
    public Post createPost(User loggedInUser, PostCreateRequest postCreateRequest) {
        Optional<User> authorOpt = userRepo.findById(loggedInUser.getId());
        Optional<Blog> blogOpt = blogRepo.findById(postCreateRequest.getBlogId());
        // User and blog have to exist
        if (authorOpt.isEmpty() || blogOpt.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        // Has to be either admin or the blog owner
        if (!(loggedInUser.isAdmin() || isBlogOwner(blogOpt.get(), loggedInUser.getId()))) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        Post post = modelMapper.map(postCreateRequest, Post.class);
        post.setBlog(blogOpt.get());

        return postRepo.save(post);
    }

    /**
     * Updates a post with given dto information
     *
     * @param loggedInUser
     * @param postUpdateRequest containing existing blog information
     */
    @Override
    @Transactional
    public void updatePost(User loggedInUser, PostUpdateRequest postUpdateRequest) {
        Optional<Post> postOpt = postRepo.findByBlogIdAndId(postUpdateRequest.getBlogId(), postUpdateRequest.getId());
        // Post has to exist
        if (postOpt.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        // Has to be either admin or the blog owner
        if (!(loggedInUser.isAdmin() || isPostOwner(postOpt.get(), loggedInUser.getId()))) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        Post post = postOpt.get();
        modelMapper.map(postUpdateRequest, post);
        postRepo.save(post);
    }

    private boolean isPostOwner(Post post, Long authorId) {
        return post.getBlog().getAuthor().getId().equals(authorId);
    }

    /**
     * Deletes a blog post based on the given blogId and postId.
     * A admin user can delete any post, whereas a normal user can delete only posts of his blog.
     *
     * @param blogId       id of the blog
     * @param postId       id of the post to be deleted
     * @param loggedInUser information about the current loggeIn user
     */
    @Override
    @Transactional
    public void deletePost(User loggedInUser, Long blogId, Long postId) {
        Optional<Post> postOpt = postRepo.findByBlogIdAndId(blogId, postId);
        if (postOpt.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        // Has to be either admin or the blog owner
        Post post = postOpt.get();
        if (!(loggedInUser.isAdmin() || isBlogOwner(post.getBlog(), loggedInUser.getId()))) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        postRepo.delete(post);
    }

    /**
     * Finds a single blog post and map to its dto before returning
     *
     * @param blogId id of the blog
     * @param postId if of the post
     * @return post dto containing restricted information
     */
    @Override
    public Optional<PostDto> findPost(Long blogId, Long postId) {
        Optional<Post> postOpt = postRepo.findByBlogIdAndId(blogId, postId);
        return postOpt.map(post -> modelMapper.map(post, PostDto.class));
    }

    /**
     * Finds blog posts in a pageable fashion
     *
     * @param pageable containing the information to be searched
     * @return page containing pagination information and list of posts of the blog
     */
    @Override
    public Page<PostDto> findPosts(Long blogId, Pageable pageable) {
        return postRepo.findByBlogId(blogId, pageable).map(post -> {
            PostDto postDto = modelMapper.map(post, PostDto.class);
            return PostUtil.truncateContent(postDto);
        });
    }

    /**
     * Find posts in a pageable fashion and maps to dto before returning.
     *
     * @param pageable containing the information to be searched
     * @return page containing pagination information and the list of posts
     */
    @Override
    public Page<PostDto> findPosts(String search, Pageable pageable) {
        if (hasText(search) && search.length() > MIN_SEARCH_LENGTH) {
            return postRepo.findByMetadataContainsIgnoreCaseOrTitleContainsIgnoreCaseOrContentContainsIgnoreCase(
                    search, search, search, pageable
            ).map(post -> modelMapper.map(post, PostDto.class));
        }
        return postRepo.findAll(pageable).map(post -> modelMapper.map(post, PostDto.class));
    }

    /**
     * Creates a comment with given dto information
     *
     * @param commentCreateRequest containing new post information
     * @return new created comment
     */
    @Override
    @Transactional
    public Comment createComment(CommentCreateRequest commentCreateRequest) {
        LoggedInUser loggedInUser = commentCreateRequest.getLoggedInUser();
        Optional<User> authorOpt = userRepo.findById(loggedInUser.getId());
        Optional<Post> postOpt = postRepo.findByBlogIdAndId(commentCreateRequest.getBlogId(), commentCreateRequest.getPostId());
        // Post and comment author have to exist
        if (authorOpt.isEmpty() || postOpt.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        Comment comment = new Comment();
        comment.setPost(postOpt.get());
        comment.setUser(authorOpt.get());
        comment.setContent(commentCreateRequest.getContent());

        return commentRepo.save(comment);
    }

    /**
     * Deletes a post comment based on the given blogId, postId and commentId.
     * A admin user can delete any post, whereas a normal user can delete only posts of his blog.
     *
     * @param blogId       id of the blog
     * @param postId       id of the post
     * @param commentId    id of the comment to be deleted
     * @param loggedInUser information about the current loggeIn user
     */
    @Override
    @Transactional
    public void deleteComment(Long blogId, Long postId, Long commentId, LoggedInUser loggedInUser) {
        Optional<Comment> commentOpt = commentRepo.findByIdAndPostIdAndPostBlogId(commentId, postId, blogId);
        if (commentOpt.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        // Has to be either admin or the blog owner
        Comment comment = commentOpt.get();
        if (!(loggedInUser.isAdmin() || isCommentOwner(comment.getUser(), loggedInUser.getId()))) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        commentRepo.delete(comment);
    }

    private boolean isCommentOwner(User commentAuthor, Long loggedInUserId) {
        return commentAuthor.getId().equals(loggedInUserId);
    }

    /**
     * Find comments in a pageable fashion and maps to dto before returning.
     *
     * @param pageable containing the information to be searched
     * @return page containing pagination information and the list of comments
     */
    @Override
    public Page<CommentDto> findComments(Long blogId, Long postId, Pageable pageable) {
        Optional<Post> postOpt = postRepo.findByBlogIdAndId(blogId, postId);
        // post has to exist
        if (postOpt.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        Page<Comment> commentsPage = commentRepo.findByPostId(postOpt.get().getId(), pageable);
        return commentsPage.map(comment -> modelMapper.map(comment, CommentDto.class));
    }
}
