package com.adobe.blog.service;

import com.adobe.blog.dto.auth.LoggedInUser;
import com.adobe.blog.dto.blog.*;
import com.adobe.blog.model.Blog;
import com.adobe.blog.model.Comment;
import com.adobe.blog.model.Post;
import com.adobe.blog.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface BlogService {
    Blog createBlog(User loggedInUser, BlogCreateRequest blogCreateRequest);
    void updateBlog(User loggedInUser, BlogUpdateRequest blogDto);
    Optional<BlogDto> findBlog(Long id);
    Page<BlogDto> findBlogs(Pageable pageable);

    Post createPost(User loggedInUser, PostCreateRequest postCreateRequest);
    void updatePost(User loggedInUser, PostUpdateRequest postUpdateRequest);
    void deletePost(User loggedInUser, Long blogId, Long postId);
    Optional<PostDto> findPost(Long blogId, Long postId);
    Page<PostDto> findPosts(Long blogId, Pageable pageable);
    Page<PostDto> findPosts(String search, Pageable pageable);

    Comment createComment(CommentCreateRequest commentCreateRequest);
    void deleteComment(Long blogId, Long postId, Long commentId, LoggedInUser loggedInUser);
    Page<CommentDto> findComments(Long blogId, Long postId, Pageable pageable);
}
