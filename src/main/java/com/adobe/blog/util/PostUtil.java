package com.adobe.blog.util;

import com.adobe.blog.dto.blog.PostDto;


public class PostUtil {
    public static final int POST_PREVIEW_SIZE = 600;

    public static PostDto truncateContent(PostDto postDto) {
        String content = postDto.getContent();
        if (content.length() > POST_PREVIEW_SIZE) {
            postDto.setContent(StringUtil.truncate(content, POST_PREVIEW_SIZE));
            postDto.setTruncated(true);
        }
        return postDto;
    }
}
