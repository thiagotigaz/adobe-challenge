package com.adobe.blog.util;

public class StringUtil {
    public static String truncate(String content, int maxSize) {
        return content.length() > maxSize
                ? content.substring(0, maxSize) + "..."
                : content;
    }
}
