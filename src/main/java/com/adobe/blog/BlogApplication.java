package com.adobe.blog;

import com.adobe.blog.dto.blog.PostDto;
import com.adobe.blog.model.Post;
import com.adobe.blog.util.PageModule;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(exclude = {UserDetailsServiceAutoConfiguration.class})
public class BlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlogApplication.class, args);
    }

    @Autowired
    public void configureJackson(ObjectMapper objectMapper) {
        objectMapper.registerModule(new PageModule());
    }

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.typeMap(Post.class, PostDto.class).addMappings(mapper -> {
            mapper.map(src -> src.getBlog().getId(), PostDto::setBlogId);
        });
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        return modelMapper;
    }
}
