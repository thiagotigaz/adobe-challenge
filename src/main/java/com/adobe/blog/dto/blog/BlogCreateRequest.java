package com.adobe.blog.dto.blog;

import com.adobe.blog.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class BlogCreateRequest {
    @JsonIgnore
    private User user;

    @NotBlank
    private String title;

    @NotBlank
    private String description;

    private String backgroundColor;

    private String primaryColor;

    private String secondaryColor;

    private String highlightColor;
}
