package com.adobe.blog.dto.blog;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
public class PostDto {
    private Long id;

    private Long blogId;

    @NotBlank
    private String title;

    @NotBlank
    private String metadata;

    @NotBlank
    private String content;

    @NotBlank
    private LocalDateTime dateCreated;

    @NotBlank
    private boolean truncated;
}
