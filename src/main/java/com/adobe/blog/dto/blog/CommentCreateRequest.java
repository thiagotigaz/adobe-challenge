package com.adobe.blog.dto.blog;

import com.adobe.blog.dto.auth.LoggedInUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommentCreateRequest {
    @JsonIgnore
    private Long blogId;

    @JsonIgnore
    private Long postId;

    @JsonIgnore
    private LoggedInUser loggedInUser;

    @NotBlank
    private String content;
}
