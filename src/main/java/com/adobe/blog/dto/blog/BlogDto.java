package com.adobe.blog.dto.blog;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BlogDto {
    private Long id;

    private String title;

    private String description;

    private LocalDateTime dateCreated;

    private String backgroundColor;

    private String primaryColor;

    private String secondaryColor;

    private String highlightColor;

    private BlogUserDto author;
}
