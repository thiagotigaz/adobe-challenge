package com.adobe.blog.dto.blog;

import com.adobe.blog.dto.auth.LoggedInUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BlogUpdateRequest {

    @JsonIgnore
    private Long id;

    @JsonIgnore
    private LoggedInUser loggedInUser;

    @NotBlank
    private String title;

    @NotBlank
    private String description;

    private String backgroundColor;

    private String primaryColor;

    private String secondaryColor;

    private String highlightColor;

}
