package com.adobe.blog.dto.blog;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostCreateRequest {
    @JsonIgnore
    private Long blogId;

    @NotBlank
    private String title;

    private String metadata;

    @NotBlank
    private String content;
}
