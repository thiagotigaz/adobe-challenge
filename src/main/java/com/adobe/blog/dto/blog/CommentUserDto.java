package com.adobe.blog.dto.blog;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommentUserDto {
    private Long id;

    private String firstName;

    private String lastName;

    private String username;

    private CommentUserBlogDto blog;
}
