package com.adobe.blog.dto.blog;

import lombok.Data;

@Data
public class BlogUserDto {
    private Long id;

    private String firstName;

    private String lastName;

    private String username;
}
