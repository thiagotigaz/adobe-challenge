package com.adobe.blog.dto.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Set;

import static com.adobe.blog.model.Role.ROLE_ADMIN;
import static com.adobe.blog.model.Role.ROLE_AUTHOR;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class LoggedInUser {
    private final Long id;
    private final String username;
    private final Set<String> authorities;
    private boolean isAdmin;
    private boolean isAuthor;

    public boolean isAdmin() {
        return authorities.contains(ROLE_ADMIN);
    }

    public boolean isAuthor() {
        return authorities.contains(ROLE_AUTHOR);
    }
}
