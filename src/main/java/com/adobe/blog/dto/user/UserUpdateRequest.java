package com.adobe.blog.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserUpdateRequest {
    @JsonIgnore
    private Long id;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    private String password;
}
