package com.adobe.blog.resource;

import com.adobe.blog.dto.blog.BlogDto;
import com.adobe.blog.dto.blog.PostDto;
import com.adobe.blog.service.BlogService;
import com.adobe.blog.util.PostUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;


@Api("Home")
@RestController
@RequestMapping("/api/v1/latest")
@RequiredArgsConstructor
public class HomeResource {
    private final BlogService blogService;

    @ApiOperation(value = "Searches for the latest 20 created blogs")
    @GetMapping("/blogs")
    List<BlogDto> latestBlogs() {
        return blogService.findBlogs(PageRequest.of(0, 20, Sort.by("dateCreated").descending()))
                .getContent();
    }

    @ApiOperation(value = "Searches for blogs in a paginable fashion")
    @GetMapping("/posts")
    List<PostDto> latestPosts(@RequestParam(required = false) String search) {
        return blogService.findPosts(search, PageRequest.of(0, 20, Sort.by("dateCreated").descending()))
                .getContent()
                .stream().map(PostUtil::truncateContent)
                .collect(Collectors.toList());
    }
}
