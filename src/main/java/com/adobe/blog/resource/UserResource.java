package com.adobe.blog.resource;

import com.adobe.blog.config.security.JwtTokenUtil;
import com.adobe.blog.dto.user.UserDto;
import com.adobe.blog.dto.user.UserCreateRequest;
import com.adobe.blog.dto.user.UserUpdateRequest;
import com.adobe.blog.exception.UsernameNotAvailableException;
import com.adobe.blog.model.User;
import com.adobe.blog.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

import static com.adobe.blog.model.Role.*;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;


@Api("User")
@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserResource {
    private final JwtTokenUtil jwtTokenUtil;
    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final ModelMapper modelMapper;

    @ApiOperation(value = "Searches for users in a paginable fashion")
    @GetMapping
    Page<UserDto> search(@SortDefault.SortDefaults({
            @SortDefault(sort = "firstName", direction = Sort.Direction.ASC)
    }) Pageable pageable) {
        return userService.findAll(pageable);
    }

    @ApiOperation(value = "Finds a user by the given id")
    @GetMapping("/{userId}")
    ResponseEntity<UserDto> findById(@PathVariable Long userId) {
        Optional<UserDto> userDto = userService.findById(userId);
        return userDto.isPresent() ? ResponseEntity.of(userDto) : ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Creates a new user with the given information")
    @PostMapping
    ResponseEntity<UserDto> createUser(
            @Valid @RequestBody UserCreateRequest userCreateRequest, HttpServletRequest request
    ) throws UsernameNotAvailableException {
        User newUser = userService.createUser(userCreateRequest);
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                userCreateRequest.getUsername(), userCreateRequest.getPassword()
        ));

        return ResponseEntity.ok()
                .header(HttpHeaders.AUTHORIZATION, jwtTokenUtil.generateAccessToken(newUser))
                .body(modelMapper.map(newUser, UserDto.class));
    }

    @ApiOperation(value = "Updates an existing user with the given information")
    @PutMapping("/{userId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Secured({ROLE_ADMIN, ROLE_USER})
    void updateUser(
            @PathVariable Long userId,
            @Valid @RequestBody UserUpdateRequest userUpdateRequest,
            @AuthenticationPrincipal User loggedInUser
    ) {
        userUpdateRequest.setId(userId);
        userService.updateUser(loggedInUser, userUpdateRequest);
    }
}
