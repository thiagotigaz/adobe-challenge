package com.adobe.blog.resource;

import com.adobe.blog.config.security.JwtTokenUtil;
import com.adobe.blog.dto.blog.PostUpdateRequest;
import com.adobe.blog.dto.blog.*;
import com.adobe.blog.model.Blog;
import com.adobe.blog.model.Comment;
import com.adobe.blog.model.Post;
import com.adobe.blog.model.User;
import com.adobe.blog.service.BlogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

import static com.adobe.blog.model.Role.*;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Api("Blog")
@RestController
@RequestMapping("/api/v1/blogs")
@RequiredArgsConstructor
public class BlogResource {
    private final BlogService blogService;
    private final JwtTokenUtil jwtTokenUtil;

    @ApiOperation(value = "Gets a list of blogs in a paginable fashion")
    @GetMapping
    Page<BlogDto> getBlogs(@SortDefault.SortDefaults({
            @SortDefault(sort = "dateCreated", direction = Sort.Direction.DESC)
    }) Pageable pageable) {
        return blogService.findBlogs(pageable);
    }

    @ApiOperation(value = "Gets a single blog based on the given blogId")
    @GetMapping("/{blogId}")
    ResponseEntity<BlogDto> getBlogById(@PathVariable Long blogId) {
        Optional<BlogDto> blogDto = blogService.findBlog(blogId);
        return blogDto.isPresent() ? ResponseEntity.of(blogDto) : ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Creates a new blog with the given information")
    @PostMapping
    @Secured({ROLE_ADMIN, ROLE_USER})
    ResponseEntity<?> createBlog(
            @Valid @RequestBody BlogCreateRequest blogCreateRequest,
            @AuthenticationPrincipal User loggedInUser,
            HttpServletRequest request
    ) {
        Blog newBlog = blogService.createBlog(loggedInUser, blogCreateRequest);
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(request.getRequestURL().toString())
                .path("/{id}")
                .buildAndExpand(newBlog.getId());
        return ResponseEntity.created(uriComponents.toUri()).build();
    }

    @ApiOperation(value = "Updates an existing blog with the given information")
    @PutMapping("/{blogId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Secured({ROLE_ADMIN, ROLE_AUTHOR})
    void updateBlog(
            @PathVariable Long blogId,
            @Valid @RequestBody BlogUpdateRequest blogUpdateRequest,
            @AuthenticationPrincipal User loggedInUser
    ) {
        blogUpdateRequest.setId(blogId);
        blogService.updateBlog(loggedInUser, blogUpdateRequest);
    }

    @ApiOperation(value = "Creates a new blog post with the given information")
    @PostMapping("/{blogId}/posts")
    @Secured({ROLE_ADMIN, ROLE_AUTHOR})
    ResponseEntity<?> createPost(
            @PathVariable Long blogId,
            @Valid @RequestBody PostCreateRequest postCreateRequest,
            @RequestHeader(name = AUTHORIZATION) String authHeader,
            @AuthenticationPrincipal User loggedInUser,
            HttpServletRequest request
    ) {
        postCreateRequest.setBlogId(blogId);
        Post newPost = blogService.createPost(loggedInUser, postCreateRequest);
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(request.getRequestURL().toString())
                .path("/{id}")
                .buildAndExpand(newPost.getId());
        return ResponseEntity.created(uriComponents.toUri()).build();
    }

    @ApiOperation(value = "Updates an existing post with the given information")
    @PutMapping("/{blogId}/posts/{postId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Secured({ROLE_ADMIN, ROLE_AUTHOR})
    void updatePost(
            @PathVariable Long blogId,
            @PathVariable Long postId,
            @Valid @RequestBody PostUpdateRequest postUpdateRequest,
            @AuthenticationPrincipal User loggedInUser
    ) {
        postUpdateRequest.setBlogId(blogId);
        postUpdateRequest.setId(postId);

        blogService.updatePost(loggedInUser, postUpdateRequest);
    }


    @ApiOperation(value = "Gets a list of posts of a blog in a pageable fashion")
    @GetMapping("/{blogId}/posts")
    Page<PostDto> getPosts(
            @PathVariable Long blogId,
            @SortDefault.SortDefaults({
                    @SortDefault(sort = "dateCreated", direction = Sort.Direction.DESC)
            }) Pageable pageable
    ) {
        return blogService.findPosts(blogId, pageable);
    }

    @ApiOperation(value = "Gets a single post of a blog")
    @GetMapping("/{blogId}/posts/{postId}")
    ResponseEntity<PostDto> getPost(@PathVariable Long blogId, @PathVariable Long postId) {
        Optional<PostDto> postDto = blogService.findPost(blogId, postId);
        return postDto.isPresent() ? ResponseEntity.of(postDto) : ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Deletes a single post of a blog based on the given blogId and postId")
    @DeleteMapping("/{blogId}/posts/{postId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Secured({ROLE_ADMIN, ROLE_AUTHOR})
    void deletePost(
            @PathVariable Long blogId,
            @PathVariable Long postId,
            @AuthenticationPrincipal User loggedInUser
    ) {
        blogService.deletePost(loggedInUser, blogId, postId);
    }

    @ApiOperation(value = "Creates a new post comment with the given information")
    @PostMapping("/{blogId}/posts/{postId}/comments")
    @Secured({ROLE_ADMIN, ROLE_AUTHOR, ROLE_USER})
    ResponseEntity<?> createComment(
            @PathVariable Long blogId,
            @PathVariable Long postId,
            @Valid @RequestBody CommentCreateRequest commentCreateRequest,
            @RequestHeader(name = AUTHORIZATION) String authHeader,
            HttpServletRequest request
    ) {
        commentCreateRequest.setBlogId(blogId);
        commentCreateRequest.setPostId(postId);
        commentCreateRequest.setLoggedInUser(jwtTokenUtil.getLoggedInUser(authHeader));
        Comment newComment = blogService.createComment(commentCreateRequest);
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(request.getRequestURL().toString())
                .path("/{id}")
                .buildAndExpand(newComment.getId());
        return ResponseEntity.created(uriComponents.toUri()).build();
    }

    @ApiOperation(value = "Gets a list of comments of a post in a pageable fashion")
    @GetMapping("/{blogId}/posts/{postId}/comments")
    Page<CommentDto> getComments(
            @PathVariable Long blogId,
            @PathVariable Long postId,
            @SortDefault.SortDefaults({
                    @SortDefault(sort = "dateCreated", direction = Sort.Direction.DESC)
            }) Pageable pageable
    ) {
        return blogService.findComments(blogId, postId, pageable);
    }

    @ApiOperation(value = "Deletes a single comment of a post based on the given blogId and postId")
    @DeleteMapping("/{blogId}/posts/{postId}/comments/{commentId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Secured({ROLE_ADMIN, ROLE_AUTHOR, ROLE_USER})
    void deleteComment(
            @PathVariable Long blogId,
            @PathVariable Long postId,
            @PathVariable Long commentId,
            @RequestHeader(name = AUTHORIZATION) String authHeader
    ) {
        blogService.deleteComment(blogId, postId, commentId, jwtTokenUtil.getLoggedInUser(authHeader));
    }
}
