package com.adobe.blog.repository;

import com.adobe.blog.dto.blog.PostDto;
import com.adobe.blog.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
    Page<Post> findByBlogId(Long blogId, Pageable pageable);

    Optional<Post> findByBlogIdAndId(Long blogId, Long postId);

    Page<Post> findByMetadataContainsIgnoreCaseOrTitleContainsIgnoreCaseOrContentContainsIgnoreCase(
            String metadata, String title, String content, Pageable pageable
    );
}
