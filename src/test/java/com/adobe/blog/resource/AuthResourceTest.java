package com.adobe.blog.resource;

import com.adobe.blog.TestDataStore;
import com.adobe.blog.dto.auth.AuthRequest;
import com.adobe.blog.dto.user.UserDto;
import com.adobe.blog.model.User;
import com.adobe.blog.util.JsonHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static com.adobe.blog.TestDataConstants.USER_PASSWORD;
import static com.adobe.blog.TestDataConstants.USER_WRONG_PASSWORD;
import static com.adobe.blog.util.JsonHelper.toJson;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class AuthResourceTest {

    private final ObjectMapper objectMapper;
    private final MockMvc mockMvc;
    private final TestDataStore testDataStore;

    @Autowired
    public AuthResourceTest(ObjectMapper objectMapper, MockMvc mockMvc, TestDataStore testDataStore) {
        this.objectMapper = objectMapper;
        this.mockMvc = mockMvc;
        this.testDataStore = testDataStore;
    }

    @Test
    public void loginShouldReturnOk() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        AuthRequest request = new AuthRequest();
        request.setUsername(user.getUsername());
        request.setPassword(USER_PASSWORD);
        // Act and Assert
        MvcResult loginResult = this.mockMvc
                .perform(post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(objectMapper, request)))
                .andExpect(status().isOk())
                .andExpect(header().exists(HttpHeaders.AUTHORIZATION))
                .andReturn();

        UserDto loggedInUser = JsonHelper.fromJson(objectMapper, loginResult.getResponse().getContentAsString(), UserDto.class);
        assertThat(loggedInUser.getId()).isEqualTo(user.getId());
    }

    @Test
    public void loginShouldReturnUnauthorizedWhenWrongPassword() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        AuthRequest request = new AuthRequest();
        request.setUsername(user.getUsername());
        request.setPassword(USER_WRONG_PASSWORD);
        // Act and Assert
        this.mockMvc
                .perform(post("/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(objectMapper, request)))
                .andExpect(status().isUnauthorized())
                .andExpect(header().doesNotExist(HttpHeaders.AUTHORIZATION))
                .andReturn();
    }
}
