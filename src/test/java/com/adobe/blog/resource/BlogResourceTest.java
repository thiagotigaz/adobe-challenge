package com.adobe.blog.resource;

import com.adobe.blog.TestDataStore;
import com.adobe.blog.dto.blog.*;
import com.adobe.blog.model.Blog;
import com.adobe.blog.model.Comment;
import com.adobe.blog.model.Post;
import com.adobe.blog.model.User;
import com.adobe.blog.service.BlogServiceImpl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.adobe.blog.TestDataConstants.*;
import static com.adobe.blog.util.JsonHelper.fromJson;
import static com.adobe.blog.util.JsonHelper.toJson;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class BlogResourceTest {

    private final ObjectMapper objectMapper;
    private final BlogServiceImpl blogService;
    private final MockMvc mockMvc;
    private final TestDataStore testDataStore;

    @Autowired
    public BlogResourceTest(ObjectMapper objectMapper, BlogServiceImpl blogService, MockMvc mockMvc, TestDataStore testDataStore) {
        this.objectMapper = objectMapper;
        this.blogService = blogService;
        this.mockMvc = mockMvc;
        this.testDataStore = testDataStore;
    }

    private static final Blog BLOG = Blog.builder()
            .id(BLOG_ID)
            .title(BLOG_TITLE)
            .description(BLOG_DESCRIPTION)
            .dateCreated(BLOG_DATE_CREATED)
            .build();
    private static final BlogDto BLOG_DTO = new BlogDto().builder()
            .id(BLOG_ID)
            .title(BLOG_TITLE)
            .description(BLOG_DESCRIPTION)
            .dateCreated(BLOG_DATE_CREATED)
            .build();

    @Test
    public void evaluatesPageableParameter() throws Exception {
        // Arrange and Act
        MvcResult getBlogsResult = mockMvc.perform(get("/api/v1/blogs")
                .param("page", "0")
                .param("size", "20")
                .param("sort", "dateCreated,desc")) // <-- no space after comma!
                .andExpect(status().isOk())
                .andReturn();
        Page<BlogDto> blogDtoPage = fromJson(
                objectMapper, getBlogsResult.getResponse().getContentAsString(), new TypeReference<>() {
                }
        );
        // Assert
        assertThat(blogDtoPage.getTotalPages()).isGreaterThanOrEqualTo(0);
        assertThat(blogDtoPage.getSize()).isEqualTo(20);
    }

    @Test
    public void getBlogByIdShouldReturnOk() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        Blog blog = testDataStore.createBlogWithUser(user);
        // Act and assert
        MvcResult getBlogResult = mockMvc.perform(get(format("/api/v1/blogs/%s", blog.getId())))
                .andExpect(status().isOk())
                .andReturn();
        BlogDto blogDto = fromJson(objectMapper, getBlogResult.getResponse().getContentAsString(), BlogDto.class);
        assertThat(blog.getId()).isEqualTo(blogDto.getId());
        assertThat(blog.getTitle()).isEqualTo(blogDto.getTitle());
        assertThat(blog.getDescription()).isEqualTo(blogDto.getDescription());
        assertThat(user.getId()).isEqualTo(blogDto.getAuthor().getId());
    }

    @Test
    public void getBlogByIdShouldReturnNotFoundWhenBlogDoesntExist() throws Exception {
        // Arrange, Act and assert
        MvcResult getBlogResult = mockMvc.perform(get(format("/api/v1/blogs/%s", INEXISTENT_BLOG_ID)))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void createBlogShouldReturnCreated() throws Exception {
        // Arrange
        User author = testDataStore.createUser();
        String token = testDataStore.getToken(author);
        BlogCreateRequest blogCreateRequest = BlogCreateRequest.builder()
                .title(BLOG_TITLE)
                .description(BLOG_DESCRIPTION)
                .build();
        // Act and assert
        mockMvc.perform(post("/api/v1/blogs")
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(objectMapper, blogCreateRequest)))
                .andExpect(status().isCreated());
    }

    @Test
    public void updateBlogShouldReturnNoContent() throws Exception {
        // Arrange
        User author = testDataStore.createUser();
        Blog blog = testDataStore.createBlogWithUser(author);
        String token = testDataStore.getToken(author);
        BlogUpdateRequest blogUpdateRequest = BlogUpdateRequest.builder()
                .title(BLOG_TITLE)
                .description(BLOG_DESCRIPTION)
                .build();
        // Act and assert
        mockMvc.perform(put(format("/api/v1/blogs/%s", blog.getId()))
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(objectMapper, blogUpdateRequest)))
                .andExpect(status().isNoContent());
    }

    @Test
    public void createPostShouldReturnCreated() throws Exception {
        // Arrange
        User author = testDataStore.createUser();
        Blog blog = testDataStore.createBlog(author.getId());
        String token = testDataStore.getToken(author);
        PostCreateRequest postCreateRequest = PostCreateRequest.builder()
                .title(POST_TITLE)
                .content(POST_CONTENT)
                .metadata(POST_METADATA)
                .build();
        // Act and assert
        mockMvc.perform(post(format("/api/v1/blogs/%s/posts", blog.getId()))
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(objectMapper, postCreateRequest)))
                .andExpect(status().isCreated());
    }

    @Test
    public void updatePostShouldReturnNoContent() throws Exception {
        // Arrange
        User author = testDataStore.createUser();
        Blog blog = testDataStore.createBlog(author.getId());
        Post post = testDataStore.createPost(author.getId(), blog.getId());
        String token = testDataStore.getToken(author);
        PostUpdateRequest postUpdateRequest = PostUpdateRequest.builder()
                .title(POST_TITLE)
                .content(POST_CONTENT)
                .metadata(POST_METADATA)
                .build();
        // Act and assert
        mockMvc.perform(put(format("/api/v1/blogs/%s/posts/%s", blog.getId(), post.getId()))
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(objectMapper, postUpdateRequest)))
                .andExpect(status().isNoContent());
    }

    @Test
    public void getPostsShouldReturnOk() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        Blog blog = testDataStore.createBlog(user.getId());
        Post post = testDataStore.createPost(user.getId(), blog.getId());
        Post post2 = testDataStore.createPost(user.getId(), blog.getId());
        Post post3 = testDataStore.createPost(user.getId(), blog.getId());
        // Act and assert
        MvcResult getPostsResult = mockMvc.perform(get(format("/api/v1/blogs/%s/posts", blog.getId())))
                .andExpect(status().isOk())
                .andReturn();
        Page<PostDto> postDtoPage = fromJson(
                objectMapper, getPostsResult.getResponse().getContentAsString(), new TypeReference<>() {
                }
        );
        assertThat(postDtoPage.getSize()).isGreaterThanOrEqualTo(3);
        List<PostDto> posts = postDtoPage.getContent();
        assertPost(posts, post);
        assertPost(posts, post2);
        assertPost(posts, post3);
    }

    private void assertPost(List<PostDto> posts, Post post) {
        Optional<PostDto> postDtoOpt = posts.stream().filter(p -> p.getId().equals(post.getId())).findFirst();
        assertThat(postDtoOpt).isNotEmpty();
        PostDto postDto = postDtoOpt.get();
        assertPost(postDto, post);
    }

    private void assertPost(PostDto postDto, Post post) {
        assertThat(postDto.getId()).isEqualTo(post.getId());
        assertThat(postDto.getTitle()).isEqualTo(post.getTitle());
        assertThat(postDto.getContent()).isEqualTo(post.getContent());
        assertThat(postDto.getMetadata()).isEqualTo(post.getMetadata());
    }

    @Test
    public void getPostShouldReturnOk() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        Blog blog = testDataStore.createBlog(user.getId());
        Post post = testDataStore.createPost(user.getId(), blog.getId());
        // Act and assert
        MvcResult getPostsResult = mockMvc.perform(
                get(format("/api/v1/blogs/%s/posts/%s", blog.getId(), post.getId())))
                .andExpect(status().isOk())
                .andReturn();
        PostDto postDto = fromJson(objectMapper, getPostsResult.getResponse().getContentAsString(), PostDto.class);
        assertPost(postDto, post);
    }

    @Test
    public void deletePostShouldReturnReturnNoContentWhenUserIsOwner() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        Blog blog = testDataStore.createBlog(user.getId());
        Post post = testDataStore.createPost(user.getId(), blog.getId());
        String token = testDataStore.getToken(user);
        // Act and assert
        mockMvc.perform(delete(format("/api/v1/blogs/%s/posts/%s", blog.getId(), post.getId()))
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    @Test
    public void deletePostShouldReturnForbiddenWhenUserIsntOwner() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        Blog blog = testDataStore.createBlog(user.getId());
        Post post = testDataStore.createPost(user.getId(), blog.getId());
        User hacker = testDataStore.createUser();
        String token = testDataStore.getToken(hacker);
        // Act and assert
        mockMvc.perform(delete(format("/api/v1/blogs/%s/posts/%s", blog.getId(), post.getId()))
                .header(AUTHORIZATION, token))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void deletePostShouldReturnReturnOkWhenUserIsAdmin() throws Exception {
        // Arrange
        User user = testDataStore.createAuthor();
        Blog blog = testDataStore.createBlog(user.getId());
        Post post = testDataStore.createPost(user.getId(), blog.getId());
        User adminUser = testDataStore.createAdminUser();
        String token = testDataStore.getToken(user);
        // Act and assert
        mockMvc.perform(delete(format("/api/v1/blogs/%s/posts/%s", blog.getId(), post.getId()))
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    @Test
    public void createCommentShouldReturnCreated() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        Blog blog = testDataStore.createBlog(user.getId());
        Post post = testDataStore.createPost(user.getId(), blog.getId());
        String token = testDataStore.getToken(user);
        CommentCreateRequest commentCreateRequest = CommentCreateRequest.builder()
                .content(COMMENT_CONTENT)
                .build();
        // Act and assert
        mockMvc.perform(post(format("/api/v1/blogs/%s/posts/%s/comments", blog.getId(), post.getId()))
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(objectMapper, commentCreateRequest)))
                .andExpect(status().isCreated())
                .andReturn();
    }

    @Test
    public void createCommentShouldUnauthorizerWhenAuthorizationIsInvalid() throws Exception {
        // Arrange
        CommentCreateRequest commentCreateRequest = CommentCreateRequest.builder()
                .content(COMMENT_CONTENT)
                .build();
        // Act and assert
        mockMvc.perform(post("/api/v1/blogs/1/posts/2/comments")
                .header(AUTHORIZATION, "Bearer INVALIDTOKEN")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(objectMapper, commentCreateRequest)))
                .andExpect(status().isUnauthorized())
                .andReturn();
    }

    @Test
    public void createCommentShouldBadRequestAuthorizationIsMissing() throws Exception {
        // Arrange, Act and assert
        mockMvc.perform(post("/api/v1/blogs/1/posts/2/comments"))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void deleteCommentShouldReturnReturnNoContentWhenUserIsOwner() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        Blog blog = testDataStore.createBlog(user.getId());
        Post post = testDataStore.createPost(user.getId(), blog.getId());
        Comment comment = testDataStore.createComment(user.getId(), blog.getId(), post.getId());
        String token = testDataStore.getToken(user);
        // Act and assert
        mockMvc.perform(delete(format("/api/v1/blogs/%s/posts/%s/comments/%s", blog.getId(), post.getId(), comment.getId()))
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    @Test
    public void deleteCommentShouldReturnReturnForbiddenWhenUserIsntOwner() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        Blog blog = testDataStore.createBlog(user.getId());
        Post post = testDataStore.createPost(user.getId(), blog.getId());
        Comment comment = testDataStore.createComment(user.getId(), blog.getId(), post.getId());
        User hacker = testDataStore.createUser();
        String token = testDataStore.getToken(hacker);
        // Act and assert
        mockMvc.perform(delete(format("/api/v1/blogs/%s/posts/%s/comments/%s", blog.getId(), post.getId(), comment.getId()))
                .header(AUTHORIZATION, token))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void deleteCommentShouldReturnReturnNoContentWhenUserIsAdmin() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        Blog blog = testDataStore.createBlog(user.getId());
        Post post = testDataStore.createPost(user.getId(), blog.getId());
        Comment comment = testDataStore.createComment(user.getId(), blog.getId(), post.getId());
        User adminUser = testDataStore.createAdminUser();
        String token = testDataStore.getToken(adminUser);
        // Act and assert
        mockMvc.perform(delete(format("/api/v1/blogs/%s/posts/%s/comments/%s", blog.getId(), post.getId(), comment.getId()))
                .header(AUTHORIZATION, token))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    @Test
    public void getCommentsShouldReturnOk() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        Blog blog = testDataStore.createBlogWithUser(user);
        Post post = testDataStore.createPost(user.getId(), blog.getId());
        Comment comment = testDataStore.createComment(user.getId(), blog.getId(), post.getId());
        Comment comment2 = testDataStore.createComment(user.getId(), blog.getId(), post.getId());
        Comment comment3 = testDataStore.createComment(user.getId(), blog.getId(), post.getId());
        Comment comment4 = testDataStore.createComment(user.getId(), blog.getId(), post.getId());
        // Act and assert
        MvcResult getPostsResult = mockMvc.perform(get(format("/api/v1/blogs/%s/posts/%s/comments", blog.getId(), post.getId())))
                .andExpect(status().isOk())
                .andReturn();
        Page<CommentDto> commentDtoPage = fromJson(
                objectMapper, getPostsResult.getResponse().getContentAsString(), new TypeReference<>() {
                }
        );
        assertThat(commentDtoPage.getSize()).isGreaterThanOrEqualTo(4);
        List<CommentDto> comments = commentDtoPage.getContent();
        assertComment(comments, comment);
        assertComment(comments, comment2);
        assertComment(comments, comment3);
        assertComment(comments, comment4);
    }

    private void assertComment(List<CommentDto> comments, Comment comment) {
        Optional<CommentDto> commentDtoOpt = comments.stream().filter(p -> p.getId().equals(comment.getId())).findFirst();
        assertThat(commentDtoOpt).isNotEmpty();
        CommentDto commentDto = commentDtoOpt.get();
        assertComment(commentDto, comment);
    }

    private void assertComment(CommentDto commentDto, Comment comment) {
        assertThat(commentDto.getId()).isEqualTo(comment.getId());
        assertThat(commentDto.getContent()).isEqualTo(comment.getContent());
    }
}
