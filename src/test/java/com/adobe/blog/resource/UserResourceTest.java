package com.adobe.blog.resource;

import com.adobe.blog.TestDataStore;
import com.adobe.blog.config.security.JwtTokenUtil;
import com.adobe.blog.dto.user.UserCreateRequest;
import com.adobe.blog.dto.user.UserDto;
import com.adobe.blog.dto.user.UserUpdateRequest;
import com.adobe.blog.model.User;
import com.adobe.blog.util.JsonHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static com.adobe.blog.TestDataConstants.*;
import static com.adobe.blog.util.JsonHelper.toJson;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserResourceTest {

    private final JwtTokenUtil jwtTokenUtil;
    private final TestDataStore testDataStore;
    private final ObjectMapper objectMapper;
    private final MockMvc mockMvc;



    @Autowired
    public UserResourceTest(JwtTokenUtil jwtTokenUtil, TestDataStore testDataStore, ObjectMapper objectMapper, MockMvc mockMvc) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.testDataStore = testDataStore;
        this.objectMapper = objectMapper;
        this.mockMvc = mockMvc;
    }

    @Test
    public void createShouldReturnOkWithContentAndAuthHeader() throws Exception {
        // Arrange
        UserCreateRequest userCreateRequest = UserCreateRequest.builder()
                .username(testDataStore.appendRandomSuffix(USER_USERNAME))
                .password(testDataStore.appendRandomSuffix(USER_PASSWORD))
                .firstName(testDataStore.appendRandomSuffix(USER_FIRSTNAME))
                .lastName(testDataStore.appendRandomSuffix(USER_LASTNAME))
                .build();
        // Act and assert
        MvcResult getUserResult = mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(objectMapper, userCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(header().exists(AUTHORIZATION))
                .andReturn();
        UserDto userDto = JsonHelper.fromJson(objectMapper, getUserResult.getResponse().getContentAsString(), UserDto.class);
        assertThat(userDto.getId()).isPositive();
        assertThat(userDto.getUsername()).isEqualTo(userCreateRequest.getUsername());
        assertThat(userDto.getFirstName()).isEqualTo(userCreateRequest.getFirstName());
        assertThat(userDto.getLastName()).isEqualTo(userCreateRequest.getLastName());
        assertThat(userDto.getDateCreated()).isNotNull();
    }

    @Test
    public void createShouldReturnConflictWhenUserAlreadyExists() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        UserCreateRequest userCreateRequest = UserCreateRequest.builder()
                .username(user.getUsername())
                .password(user.getPassword())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .build();
        // Act and assert
        mockMvc.perform(post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(objectMapper, userCreateRequest)))
                .andExpect(status().isConflict());
    }

    @Test
    public void findByIdShouldReturnOk() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        // Act and assert
        MvcResult getUserResult = mockMvc.perform(get(format("/api/v1/users/%s", user.getId())))
                .andExpect(status().isOk())
                .andReturn();
        UserDto userDto = JsonHelper.fromJson(objectMapper, getUserResult.getResponse().getContentAsString(), UserDto.class);
        assertThat(userDto.getId()).isEqualTo(user.getId());
        assertThat(userDto.getUsername()).isEqualTo(user.getUsername());
        assertThat(userDto.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(userDto.getLastName()).isEqualTo(user.getLastName());
        assertThat(userDto.getDateCreated()).isEqualTo(user.getDateCreated());
    }

    @Test
    public void findByIdShouldReturnNotFoundWhenUserDoesntExist() throws Exception {
        // Act and assert
        mockMvc.perform(get(format("/api/v1/users/%s", INEXISTENT_USER_ID)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateShouldReturnForbiddenWhenUserIsValid() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        UserUpdateRequest userCreateRequest = UserUpdateRequest.builder()
                .password(USER_PASSWORD)
                .firstName("Modified first name")
                .lastName(user.getLastName())
                .build();
        User loggedInUser = testDataStore.createUser();
        String token = testDataStore.getToken(loggedInUser);
        // Act and assert
        MvcResult getUserResult = mockMvc.perform(put(format("/api/v1/users/%s", user.getId()))
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(objectMapper, userCreateRequest)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void updateShouldReturnNoContentWhenUserValid() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        UserUpdateRequest userCreateRequest = UserUpdateRequest.builder()
                .password(USER_PASSWORD)
                .firstName("Modified first name")
                .lastName(user.getLastName())
                .build();
        String token = testDataStore.getToken(user);
        // Act and assert
        MvcResult getUserResult = mockMvc.perform(put(format("/api/v1/users/%s", user.getId()))
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(objectMapper, userCreateRequest)))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    @Test
    public void updateShouldReturnNoContentWhenUserIsAdmin() throws Exception {
        // Arrange
        User user = testDataStore.createUser();
        UserUpdateRequest userCreateRequest = UserUpdateRequest.builder()
                .id(user.getId())
                .password(USER_PASSWORD)
                .firstName("Modified first name")
                .lastName(user.getLastName())
                .build();

        User admin = testDataStore.createAdminUser();
        String token = testDataStore.getToken(admin);
        // Arrange, Act and assert
        mockMvc.perform(put(format("/api/v1/users/%s", user.getId()))
                .header(AUTHORIZATION, token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(objectMapper, userCreateRequest)))
                .andExpect(status().isNoContent());
    }
}
