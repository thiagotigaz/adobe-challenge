package com.adobe.blog;

import com.adobe.blog.config.security.JwtTokenUtil;
import com.adobe.blog.dto.auth.LoggedInUser;
import com.adobe.blog.dto.blog.BlogCreateRequest;
import com.adobe.blog.dto.blog.CommentCreateRequest;
import com.adobe.blog.dto.blog.PostCreateRequest;
import com.adobe.blog.dto.user.UserCreateRequest;
import com.adobe.blog.dto.user.UserDto;
import com.adobe.blog.model.*;
import com.adobe.blog.repository.RoleRepository;
import com.adobe.blog.repository.UserRepository;
import com.adobe.blog.service.BlogServiceImpl;
import com.adobe.blog.service.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.adobe.blog.TestDataConstants.*;
import static java.lang.String.format;
import static java.lang.System.currentTimeMillis;

@Service
@RequiredArgsConstructor
public class TestDataStore {

    private final RoleRepository roleRepo;
    private final UserRepository userRepo;
    private final UserServiceImpl userService;
    private final BlogServiceImpl blogService;
    private final JwtTokenUtil jwtTokenUtil;


    public static UserDto USER_DTO = UserDto.builder()
            .id(USER_ID)
            .username(USER_USERNAME)
            .firstName(USER_FIRSTNAME)
            .lastName(USER_LASTNAME)
            .dateCreated(USER_DATECREATED)
            .build();

    public static final long ROLE_ADMIN_ID = 1L;
    public static final long ROLE_USER_ID = 2L;
    public static final long ROLE_AUTHOR_ID = 3L;
    public static Role ROLE_USER = Role.builder().id(ROLE_USER_ID).authority(Role.ROLE_USER).build();
    public static Role ROLE_ADMIN = Role.builder().id(ROLE_ADMIN_ID).authority(Role.ROLE_ADMIN).build();
    public static Role ROLE_AUTHOR = Role.builder().id(ROLE_AUTHOR_ID).authority(Role.ROLE_AUTHOR).build();

    public static User USER = User.builder()
            .id(USER_ID)
            .username(USER_USERNAME)
            .firstName(USER_FIRSTNAME)
            .lastName(USER_LASTNAME)
            .dateCreated(USER_DATECREATED)
            .authorities(Arrays.asList(ROLE_USER))
            .build();

    public static User AUTHOR = User.builder()
            .id(USER_ID)
            .username(USER_USERNAME)
            .firstName(USER_FIRSTNAME)
            .lastName(USER_LASTNAME)
            .dateCreated(USER_DATECREATED)
            .authorities(Arrays.asList(ROLE_USER, ROLE_AUTHOR))
            .build();

    public static User ADMIN = User.builder()
            .id(USER_ID)
            .username(USER_USERNAME)
            .firstName(USER_FIRSTNAME)
            .lastName(USER_LASTNAME)
            .dateCreated(USER_DATECREATED)
            .authorities(Arrays.asList(ROLE_ADMIN, ROLE_USER))
            .build();

    public static final UserCreateRequest USER_CREATE_REQUEST = UserCreateRequest.builder()
            .username(USER_USERNAME)
            .password(USER_PASSWORD)
            .firstName(USER_FIRSTNAME)
            .lastName(USER_LASTNAME)
            .build();

    public static final BlogCreateRequest BLOG_CREATE_REQUEST = BlogCreateRequest.builder()
            .title(BLOG_TITLE)
            .description(BLOG_DESCRIPTION)
            .build();

    public static final PostCreateRequest POST_CREATE_REQUEST = PostCreateRequest.builder()
            .title(POST_TITLE)
            .content(POST_CONTENT)
            .metadata(POST_METADATA)
            .build();

    public static final CommentCreateRequest COMMENT_CREATE_REQUEST = CommentCreateRequest.builder()
            .content(COMMENT_CONTENT)
            .build();

    public String appendRandomSuffix(String s) {
        return format("%s%s", s, currentTimeMillis());
    }

    public User createUser() throws Exception {
        UserCreateRequest userCreateRequest = USER_CREATE_REQUEST.toBuilder()
                .username(appendRandomSuffix(USER_USERNAME)).build();
        return userService.createUser(userCreateRequest);
    }

    public User createAuthor() throws Exception {
        User user = createUser();
        user = userRepo.findById(user.getId()).get();
        List<Role> authorities = user.getAuthorities();
        List<Role> newAuthorities = new ArrayList<>(authorities);
        newAuthorities.add(roleRepo.findByAuthority(Role.ROLE_AUTHOR));
        user.setAuthorities(newAuthorities);
        return userRepo.save(user);
    }

    public User createAdminUser() throws Exception {
        User user = createUser();
        user = userRepo.findById(user.getId()).get();
        List<Role> authorities = user.getAuthorities();
        List<Role> newAuthorities = new ArrayList<>(authorities);
        newAuthorities.add(roleRepo.findByAuthority(Role.ROLE_ADMIN));
        user.setAuthorities(newAuthorities);
        return userRepo.save(user);
    }

    public Blog createBlog(Long userId) {
        User loggedInUser = userRepo.findById(userId).get();
        return blogService.createBlog(loggedInUser, BLOG_CREATE_REQUEST);
    }

    public Blog createBlogWithUser(User user) {
        return blogService.createBlog(user, BLOG_CREATE_REQUEST);
    }

    public String getToken(User user) {
        return format(BEARER_TOKEN_FORMAT, jwtTokenUtil.generateAccessToken(user));
    }

    public Post createPost(Long userId, Long blogId) {
        User user = userRepo.findById(userId).get();
        POST_CREATE_REQUEST.setTitle(appendRandomSuffix(POST_TITLE));
        POST_CREATE_REQUEST.setContent(appendRandomSuffix(POST_CONTENT));
        POST_CREATE_REQUEST.setMetadata(appendRandomSuffix(POST_METADATA));
        POST_CREATE_REQUEST.setBlogId(blogId);
        return blogService.createPost(user, POST_CREATE_REQUEST);
    }

    public Comment createComment(Long userId, Long blogId, Long postId) {
        COMMENT_CREATE_REQUEST.setLoggedInUser(
                LoggedInUser.builder().id(userId).build()
        );
        COMMENT_CREATE_REQUEST.setContent(appendRandomSuffix(COMMENT_CONTENT));
        COMMENT_CREATE_REQUEST.setBlogId(blogId);
        COMMENT_CREATE_REQUEST.setPostId(postId);
        return blogService.createComment(COMMENT_CREATE_REQUEST);
    }
}
