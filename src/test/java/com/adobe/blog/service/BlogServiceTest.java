package com.adobe.blog.service;

import com.adobe.blog.TestDataStore;
import com.adobe.blog.dto.auth.LoggedInUser;
import com.adobe.blog.dto.blog.BlogCreateRequest;
import com.adobe.blog.dto.blog.BlogUpdateRequest;
import com.adobe.blog.dto.blog.CommentCreateRequest;
import com.adobe.blog.dto.blog.PostCreateRequest;
import com.adobe.blog.model.Blog;
import com.adobe.blog.model.Post;
import com.adobe.blog.model.Role;
import com.adobe.blog.model.User;
import com.adobe.blog.repository.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

import static com.adobe.blog.TestDataConstants.*;
import static com.adobe.blog.TestDataStore.USER;
import static com.adobe.blog.model.Role.ROLE_USER;
import static com.adobe.blog.service.BlogServiceImpl.THE_AUTHOR_ALREADY_HAS_A_BLOG_ONLY_ONE_BLOG_PER_USER_IS_ALLOWED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.*;

@ExtendWith(MockitoExtension.class)
public class BlogServiceTest {
    @Mock
    private UserRepository userRepo;

    @Mock
    private BlogRepository blogRepo;

    @Mock
    private PostRepository postRepo;

    @Mock
    private CommentRepository commentRepo;

    @Mock
    private RoleRepository roleRepo;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private BlogServiceImpl blogService;

    @Test
    public void createBlogSuccess() {
        // Arrange
        BlogCreateRequest blogCreateRequest = new BlogCreateRequest();
        User user = USER.toBuilder().authorities(new ArrayList<>(USER.getAuthorities())).build();
        Blog blog = mock(Blog.class);
        when(userRepo.findById(user.getId())).thenReturn(Optional.of(user));
        when(modelMapper.map(blogCreateRequest, Blog.class)).thenReturn(blog);
        // Act
        blogService.createBlog(user, blogCreateRequest);
        // Assert
        verify(blog).setAuthor(user);
        verify(blogRepo).save(blog);
    }

    @Test
    public void createBlogShouldReturnNotFoundWhenUserIsNotFound() {
        // Arrange
        BlogCreateRequest blogCreateRequest = new BlogCreateRequest();
        when(userRepo.findById(USER.getId())).thenReturn(Optional.empty());
        // Act
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            blogService.createBlog(USER, blogCreateRequest);
        });
        // Assert
        assertThat(exception.getStatus()).isEqualTo(NOT_FOUND);
    }

    @Test
    public void createBlogShouldReturnBadRequestWhenUserAlreadyHasABlog() {
        // Arrange
        User user = mock(User.class);
        when(user.getId()).thenReturn(USER_ID);
        BlogCreateRequest blogCreateRequest = BlogCreateRequest.builder()
                .build();
        Blog blog = mock(Blog.class);
        when(userRepo.findById(USER_ID)).thenReturn(Optional.of(user));
        when(user.getBlog()).thenReturn(blog);
        // Act
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            blogService.createBlog(user, blogCreateRequest);
        });
        // Assert
        assertThat(exception.getStatus()).isEqualTo(BAD_REQUEST);
        assertThat(exception.getReason()).isEqualTo(THE_AUTHOR_ALREADY_HAS_A_BLOG_ONLY_ONE_BLOG_PER_USER_IS_ALLOWED);
    }

    @Test
    public void testIsBlogOwner() {
        // Arrange, Act and Assert
        assertThat(BlogServiceImpl.isBlogOwner(
                Blog.builder().author(User.builder().id(USER_ID).build()).build(), USER_ID)
        ).isTrue();
        assertThat(BlogServiceImpl.isBlogOwner(
                Blog.builder().author(User.builder().id(USER_ID).build()).build(), USER_ANOTHER_ID)
        ).isFalse();
    }

    @Test
    public void updateBlogSuccess() {
        // Arrange
        BlogUpdateRequest blogUpdateRequest = BlogUpdateRequest.builder()
                .id(BLOG_ID)
                .loggedInUser(LoggedInUser.builder().id(USER_ID).authorities(Set.of(ROLE_USER)).build())
                .build();
        User author = mock(User.class);
        Blog blog = mock(Blog.class);
        when(blogRepo.findById(BLOG_ID)).thenReturn(Optional.of(blog));
        when(blog.getAuthor()).thenReturn(author);
        when(author.getId()).thenReturn(USER_ID);
        // Act
        blogService.updateBlog(author, blogUpdateRequest);
        // Assert
        verify(blogRepo).findById(BLOG_ID);
        verify(modelMapper).map(blogUpdateRequest, blog);
        verify(blogRepo).save(blog);
    }

    @Test
    public void updateBlogShouldReturnNotFoundIfBlogDoestExist() {
        // Arrange
        User user = mock(User.class);
        BlogUpdateRequest blogUpdateRequest = BlogUpdateRequest.builder().id(BLOG_ID).build();
        when(blogRepo.findById(BLOG_ID)).thenReturn(Optional.empty());
        // Act
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            blogService.updateBlog(user, blogUpdateRequest);
        });
        // Assert
        verify(blogRepo).findById(BLOG_ID);
        assertThat(exception.getStatus()).isEqualTo(NOT_FOUND);
    }

    @Test
    public void updateBlogShouldBadRequestIfUserIsntOwner() {
        // Arrange
        BlogUpdateRequest blogUpdateRequest = BlogUpdateRequest.builder()
                .id(BLOG_ID)
                .loggedInUser(LoggedInUser.builder().id(USER_ID).authorities(Set.of(ROLE_USER)).build())
                .build();
        User user = mock(User.class);
        Blog blog = mock(Blog.class);
        when(blogRepo.findById(BLOG_ID)).thenReturn(Optional.of(blog));
        when(blog.getAuthor()).thenReturn(User.builder().id(USER_ANOTHER_ID).build());
        // Act
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            blogService.updateBlog(user, blogUpdateRequest);
        });
        // Assert
        verify(blogRepo).findById(BLOG_ID);
        assertThat(exception.getStatus()).isEqualTo(FORBIDDEN);
    }

    @Test
    public void createPostSuccess() {
        // Arrange
        PostCreateRequest postCreateRequest = PostCreateRequest.builder()
                .blogId(BLOG_ID)
//                .loggedInUser(LoggedInUser.builder().id(USER_ID).authorities(Set.of(ROLE_USER)).build())
                .title(POST_TITLE)
                .content(POST_CONTENT)
                .metadata(POST_METADATA)
                .build();
        Blog blog = mock(Blog.class);
        User author = User.builder().id(USER_ID).build();
        Post post = mock(Post.class);
        when(userRepo.findById(USER_ID)).thenReturn(Optional.of(author));
        when(blogRepo.findById(BLOG_ID)).thenReturn(Optional.of(blog));
        when(blog.getAuthor()).thenReturn(author);
        when(modelMapper.map(postCreateRequest, Post.class)).thenReturn(post);
        // Act
        blogService.createPost(USER, postCreateRequest);
        // Assert
        verify(userRepo).findById(USER_ID);
        verify(blogRepo).findById(BLOG_ID);
        verify(postRepo).save(post);
    }

    @Test
    public void createPostShouldReturnUnauthorizedIfUserIsntAuthor() {
        // Arrange
        PostCreateRequest postCreateRequest = PostCreateRequest.builder()
                .blogId(BLOG_ID)
                .title(POST_TITLE)
                .content(POST_CONTENT)
                .metadata(POST_METADATA)
                .build();
        Blog blog = mock(Blog.class);
        User author = User.builder().id(USER_ANOTHER_ID).build();
        when(userRepo.findById(USER_ID)).thenReturn(Optional.of(author));
        when(blogRepo.findById(BLOG_ID)).thenReturn(Optional.of(blog));
        when(blog.getAuthor()).thenReturn(author);
        // Act
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            blogService.createPost(USER, postCreateRequest);
        });
        // Assert
        verify(userRepo).findById(USER_ID);
        verify(blogRepo).findById(BLOG_ID);
        assertThat(exception.getStatus()).isEqualTo(FORBIDDEN);
    }

    @Test
    public void createCommentSuccess() {
        // Arrange
        CommentCreateRequest commentCreateRequest = CommentCreateRequest.builder()
                .blogId(BLOG_ID)
                .postId(POST_ID)
                .loggedInUser(LoggedInUser.builder().id(USER_ID).authorities(Set.of(ROLE_USER)).build())
                .content(COMMENT_CONTENT)
                .build();
        User author = mock(User.class);
        Post post = mock(Post.class);
        when(userRepo.findById(USER_ID)).thenReturn(Optional.of(author));
        when(postRepo.findByBlogIdAndId(BLOG_ID, POST_ID)).thenReturn(Optional.of(post));
        // Act
        blogService.createComment(commentCreateRequest);
        // Assert
        verify(userRepo).findById(USER_ID);
        verify(postRepo).findByBlogIdAndId(BLOG_ID, POST_ID);
        verify(commentRepo).save(any());
    }
}
