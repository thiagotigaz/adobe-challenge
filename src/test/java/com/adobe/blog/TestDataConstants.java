package com.adobe.blog;

import java.time.LocalDateTime;

public class TestDataConstants {
    public static final Long USER_ID = 1L;
    public static final Long USER_ANOTHER_ID = 2L;
    public static final String USER_USERNAME = "thiago";
    public static final String USER_PASSWORD = "thiago12345";
    public static final String USER_WRONG_PASSWORD = "WRONG_PASSWORD";
    public static final String USER_FIRSTNAME = "Thiago";
    public static final String USER_LASTNAME = "Lima";
    public static final LocalDateTime USER_DATECREATED = LocalDateTime.now();
    public static final String BLOG_TITLE = "Sample Title";
    public static final String BLOG_DESCRIPTION = "Sample Description";
    public static final Long INEXISTENT_USER_ID = 4321L;
    public static final Long INEXISTENT_BLOG_ID = 4321L;
    public static final String BEARER_TOKEN_FORMAT = "Bearer %s";
    public static final String POST_TITLE = "sample blog title";
    public static final String POST_CONTENT = "sample blog content";
    public static final String POST_METADATA = "meta;data";
    public static final String COMMENT_CONTENT = "this is a sample post commentary";
    public static final Long BLOG_ID = 100L;
    public static final Long POST_ID = 101L;
    public static final LocalDateTime BLOG_DATE_CREATED = LocalDateTime.now();
}
