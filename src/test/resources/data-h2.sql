MERGE INTO public.role KEY (ID) VALUES (10000, 'ROLE_ADMIN');
MERGE INTO public.role KEY (ID) VALUES (20000, 'ROLE_USER');
MERGE INTO public.role KEY (ID) VALUES (30000, 'ROLE_AUTHOR');

MERGE INTO public.user_account KEY (ID) VALUES (40000, '2021-07-25 17:03:42.113324', NULL, 'Admin Fn', 'Admin Ln', '$2a$10$cJYNA4945rjtsDE..HgRi.WtO0.Dj65X1CfPtY9EY.R/DsQvT6/UK', 'admin');
MERGE INTO public.user_account KEY (ID) VALUES (50000, '2021-07-25 17:08:55.814744', NULL, 'Thiago', 'Lima', '$2a$10$TDbpvRsrphHzQqJO3/oheuGL.i6hlD.xdAazf895lf9ZO4KArmQR.', 'thiago');
MERGE INTO public.user_account KEY (ID) VALUES (60000, '2021-07-26 11:05:17.912475', NULL, 'Talita', 'Pinto', '$2a$10$q4ztQRkL03buN/BWJ3k8FO0ofE6EdEAGxY0LSHxwEfeWRsz49tgDK', 'talita');

INSERT INTO public.user_role(user_id, role_id) SELECT 40000, 10000 WHERE NOT EXISTS (SELECT 1 FROM user_role WHERE user_id = 40000 AND role_id = 10000);
INSERT INTO public.user_role(user_id, role_id) SELECT 40000, 20000 WHERE NOT EXISTS (SELECT 1 FROM user_role WHERE user_id = 40000 AND role_id = 20000);
INSERT INTO public.user_role(user_id, role_id) SELECT 50000, 20000 WHERE NOT EXISTS (SELECT 1 FROM user_role WHERE user_id = 50000 AND role_id = 20000);
INSERT INTO public.user_role(user_id, role_id) SELECT 50000, 30000 WHERE NOT EXISTS (SELECT 1 FROM user_role WHERE user_id = 50000 AND role_id = 30000);
INSERT INTO public.user_role(user_id, role_id) SELECT 60000, 20000 WHERE NOT EXISTS (SELECT 1 FROM user_role WHERE user_id = 60000 AND role_id = 20000);
INSERT INTO public.user_role(user_id, role_id) SELECT 60000, 30000 WHERE NOT EXISTS (SELECT 1 FROM user_role WHERE user_id = 60000 AND role_id = 30000);

