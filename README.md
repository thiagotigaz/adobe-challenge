# ADOBE-CHALLENGE
Spring/React blog project for Adobe code challenge. This app is composed of a spring boot api(http://localhost:8081), a node js react application(http://localhost:5000) and a postgresql database.

# Entity Relation Diagram
![Entity Relation Diagram](./docs/erd.png "Component Diagram")


# Api Documentation
To check Api's swagger documentation go to http://localhost:8081/swagger-ui

# How to build and run
#### Software Needed:
* docker
* jdk 8+ (optional, see `Alternative build` below)
* node 14+ (optional, running on docker)

First we need to build spring-boot docker container and then start docker-compose. The ui node application has it's own Dockerfile and is built during docker-compose startup.

```
./mvnw spring-boot:build-image
docker-compose up
```
### Alternative build 
In case you don't have java installed in your machine you can build the spring-boot container inside docker using an `openjdk` container:
```
# Build spring boot docker image inside openjdk container
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v "$PWD":/usr/src/myapp -w /usr/src/myapp openjdk ./mvnw spring-boot:build-image
# Start postgresql, node and sprint-boot
docker-compose up
```

# Sample Blog Users
Username and password are separated by `':'`. The admin account allows user to `update/delete` resources of other accounts. For example updating another user's blog, deleting another user comment's.
```
admin:admin12345 - (ADMIN_ROLE, USER_ROLE)
thiago:thiago12345 - (USER_ROLE, AUTHOR_ROLE)
talita:talita12345 - (USER_ROLE, AUTHOR_ROLE)
```

# Local Development
For local development you need at least jdk 8 and node js 14 installed, and postgresql running. You can start only postgres (not all 3 containers) running
```
docker-compose run -p5432:5432 postgres
```

### Backend / Spring boot local development
Make sure database is running and exposed on localhost:5432. The following command will start the spring boot application on http://localhost:8080
```
./mvnw spring-boot:run
```

### Frontend / Node React local development
The following commands will download the node dependencies and start development server under http://localhost:3000 
```
cd blog-ui
npm i
npm start
```